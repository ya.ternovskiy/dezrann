let iotl = require('./ioTestLib.js')

const NB_MVTS = 5000

async function execute () {
  label = new iotl.Label ({id : "label1"})
  await iotl.initialisation([label])
  let client = new iotl.Client(1)
  await client.connect()
  await client.setLabel("label1")

  let ok1 = 0
  client.on("accept_channel", data => {
    client.validate(data.current_operation.op)
    ok1++
    client.version = data.current_operation.version
    if (ok1 == NB_MVTS) {
      client.disconnect()
    }
  })

  client.on("deny_channel", data => {
    client.emit('analysis-demande', {})
  })

  client.on("change-analysis", data => {
    label._start = data.current_operation.op.changes.start
  })

  client.on('analysis-channel', (data) => {
    client.version = data.current_version
    label._start = data.current_analysis[0].start
  })


  await client.multiMovesLabel(NB_MVTS, 0.01, "op1", "change1")
}

execute()
