
## How to run the application?
#####	cd cmt-server
#####	npm start
#####	open chrom --localhost:3000/demo
#####	open chrom --localhost:3000/demo
#####	open chrom ......... as many as you want.

## Rapport
###	Semaine before NOEL Vacance
#### prepare for the PJI. Create socket-chat version-simple and learn to read and write data by fs module.
	
###	Semaine 1.16-1.22
####	
	Description: Create a simple project with polymer and with server-end with favicon.ico.
	Learned: Polymer grammar, including 
			1)	bind :'[[a]]','{{b}}',complet data-binding: this.push('comment',{object}); 
			2)	key word:'this'; 
			3)	define variable (this.xxx), local variable(let);
			4)	function: ready(), properties(),constructure().....
### Semaine 1.23-1.30
####
	Description: Add css decolaration. Add json to init comment in front-end. Add fs module to handle stockage in back-end. Change to use socket.broadcast to emit msg.
	Learned: Polymer components and handle file.
			1) For css: Polymer components <papar-input/button> <iron-icons>,and flex-layout to draw form.
			2) For front-end init comment: fetch();
			3) For back-end: fs module to handle stockage of msg
			4) For socket.io: change to use broadcast to emit msg to every user.
	Problem and solution
		1) When you reopen the website, you can not find ancient comments wirtten by other times. Later user can not see the ancient comments.
			cause: every time the array will be loaded and inisialized vide.
			solve: set up a comment.json as a database to init the comment Array.
		2) New message writen and stocked is complet.
			cause: Front End is too heavy if it need to write and read together. Every user will need to read and write a time.
			solve: Use fs in back-end to write and read to handle the new message. Front-end just init from database and never handle the new message. Very easy to understand.
		3) New message didn't load at real time. You need to refresh the website to load latest new.
			cause: socket function error. only the writer himself can see the message update.
			solve: change to use socket.broadcast function. and render immediately the latest message to [[comment]], so everybody can receive immediately.

