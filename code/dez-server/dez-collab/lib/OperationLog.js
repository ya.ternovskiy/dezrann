var Analysis = require('./Analysis')

class OperationLog {

	constructor () {
		this._log = []
		this._version = 0
	}

	addOperation (operation) {
		this._version++;
		operation.version = this._version;
		this._log.push(operation)
		return operation
	}

	get operations () {
		return  this._log.filter(o => true)
	}

	get version () {
		return this._version
	}

	generateAnalysisByOperation () {
		let analysis = new Analysis()
		this.operations.forEach(operation => {
			if (operation.op.action == "REMOVE") {
				if (analysis.getLabel(operation.op.labelId) != undefined) {
					analysis.removeLabel(operation.op.labelId)
				}
			} else if (operation.op.action == "CREATE") {
				let newLabel = operation.op.changes
				newLabel.id = operation.op.labelId
				analysis.addLabel(newLabel)
			} else {
				let label = analysis.getLabelById(operation.op.labelId)
				Object.keys(operation.op.changes).forEach(key => {
					if (key != "id") {
						let value = operation.op.changes[key]
						if (key == "line") {
							let lineTab = value.split('.')
							if (lineTab[0] == 'staves') {
								label.staff = parseInt(lineTab[1])
							} else {
								delete label.staff
							}
						}
						label[key] = value
					}
				})
				analysis.replace(operation.op.labelId,label)
			}
		})
		return analysis
	}

}

module.exports = OperationLog;
