class Operation {

	constructor (operation, socketId) {
		this.version = undefined
		this.socketId = socketId
		this.op = operation
	}

}

module.exports = Operation;
