class Analysis {

	constructor () {
		this._analysis = []
	}

	addLabel (label) {
		this._analysis.push(label)
	}

	get labels () {
		return this._analysis
	}

	getLabelById (id) {
		let copy = {}
		let label = this._analysis.filter(l => l.id == id)[0]
		Object.keys(label).forEach(key => {
			copy[key] = label[key]
		})
		copy.end = label.end
		return copy
	}

	removeLabel (id) {
    this._analysis = this._analysis.filter(label => label.id != id)
  }

	getLabel (id) {
		return this._analysis.filter(label => label.id == id) [0]
	}

	replace (id, label) {
		this.removeLabel(id)
		this.addLabel(label)
	}

}

module.exports = Analysis;
