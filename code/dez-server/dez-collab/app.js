let path = require('path');
let http = require('http');
let connect = require('connect');
let io = require('socket.io');
let fs = require('fs');
let Operation = require('./lib/Operation')
let OperationLog = require('./lib/OperationLog')

//change clientdir here
let clientdir = path.join(process.cwd(), '../');
let app = connect().use(connect.static(clientdir));
let server = http.createServer(app);
let origin_version = '02-reference.dez';
let log_file = 'log_file.json';
let broadcast_version = [];
let logs = [];

function updateLogAndBroadcast (op, id, socket) {
	let update_operation = new Operation(op, id);
	operationLog.addOperation(update_operation);
	socket.broadcast.emit('change-analysis', {
		current_operation : update_operation
	});
	socket.emit('accept_channel', {
		current_operation : update_operation
	});
}
function commutative (operation, version, socketId) {
	for (let op of operationLog.operations.filter(o => o.version > version)) {
		if (op.op.labelId == operation.labelId) {
			for (let key of Object.keys(op.op.changes).filter(k => k != "id")) {
				if (Object.keys(operation.changes).includes(key)) {
					if (op.socketId != socketId) {
						return false
					}
				}
			}
		}
	}
	return true
}

//read log_file
fs.exists(log_file, exists => {
	if(!exists) {
		fs.open(log_file, "w", (err, fd) => {});
	} else {
		fs.unlink(log_file, (err,fd) => {});
	}
})

let socketServer = io.listen(server);
socketServer.set('log level',0);

let operationLog = new OperationLog()

socketServer.sockets.on('connection', socket => {
	let ip = socket.handshake.address.address;
	let id = socket.id
	console.log('Socket connection. ' + ip, id);
	//handle discnnect
	socket.on('disconnect', client => {
		console.log('Socket disconnect. ' + ip, id);
	});
	//if new client connect
	socket.on('analysis-demande', data => {
		analysis=operationLog.generateAnalysisByOperation()
		socket.emit('analysis-channel', {
			current_analysis : analysis.labels,
			current_version : operationLog.version
		});
	})
	//dez-collab channel
	socket.on('update-label', data => {
		if (data.server_version == operationLog.version) {
			console.log("INFO : update log", data.client_version, data.server_version, operationLog.version);
			updateLogAndBroadcast(data.op, id, socket)
		} else if (data.server_version < operationLog.version) {
			console.log("WARNING : client is late from the server", data.server_version, operationLog.version);
			if (commutative(data.op, data.server_version, id)) {
				updateLogAndBroadcast(data.op, id, socket)
			} else {
				socket.emit('deny_channel', {
					last_op : data.op
				})
			}
		} else {
			console.log("ERROR : client has a version number too high !", data.server_version, operationLog.version);
		}
	});
	socket.on('remove-label', data => {
		updateLogAndBroadcast(data.op, id, socket)
	});
	socket.on('create-label', data => {
		updateLogAndBroadcast(data.op, id, socket)
	});

	// needed for fonctional tests
	socket.on('reinit', () => {
		operationLog = new OperationLog()
	})
});

server.listen(3000, () => {
	console.log('server started on localhost:3000, please visit this adresse');
});
