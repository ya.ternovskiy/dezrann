var ratio; 
var group;
var h;
var w;
var startX;
var startY;
var currentRect;
var move = false;
var arrowStartX;
var arrowStartY;
var currentWidth;
var currentX;
 
var setUpListeners = function() {
    //TODO : si je suis sur mobile => window.innerHeight
    // si je suis sur tablette on divise la taille par 1.5
    var image = new Image();
    image.src = "02.png";
    image.addEventListener("load",function(event){
        Snap('#zoom').attr({
            height:this.naturalHeight,
            width:this.naturalWidth
        });

        ratio = this.naturalWidth/window.innerWidth;

        Snap("#full").attr({
            height: this.naturalHeight/ratio,
            width:window.innerWidth
        });

        var paper = Snap('#zoom');

        group = paper.g();
        group.attr({transform:'matrix(1,0,0,1,0,0)'});

        var img = paper.image("02.png",0,0,paper.attr("width"),paper.attr("height")).attr({
                     id:"detail"});

        group.add(img);

        img.touchstart(startTouch);
        img.touchmove(manageDirection)

        var global = Snap('#full');
    
        var view = global.image("02.png",0,0,"100%","100%").attr({
                    id:"global",
                    preserveAspectRatio:"xMinYMin"
                });

        var frame = global.rect(5,0,(1/ratio)*100 + "%","100%").attr({
                "fill-opacity":0,
                stroke: "#000",
                strokeWidth:1,
                id : "frame"
            });

        frame.touchmove(fixFrameTouch);
    });
}

window.addEventListener("load",setUpListeners)

var fixFrameTouch = function(event) {
    event.preventDefault();
    var x = event.touches[0].pageX; 

    //set the cursor to the middle of the rectangle 
    var frameWidth = this.node.width.baseVal.value/2;

    if (x <0) {
        x = frameWidth; 
    }

    if (x + frameWidth > window.innerWidth) {
        x = window.innerWidth - frameWidth; 
    }

    var y = x - frameWidth

    this.attr({x: x - frameWidth});

    group.attr({transform:"matrix(1,0,0,1"+ (-(y*ratio)) + ",0)"});
}

var stopSelection = function() {
    try {
        currentRect.attr({"fill-opacity":0.3});
        Snap('#zoom').selectAll('.arrow').forEach(function(e){
            e.remove();
        },this);
    }
    catch (e) {
         
    }
}


var startTouch = function(event) {
    event.preventDefault();
    start = event.timeStamp;
    startX = event.changedTouches[0].pageX;
    startY = event.changedTouches[0].pageY;
    stopSelection();
     var offset = group.attr("transform").globalMatrix.e;
    currentRect = group.rect(startX - offset ,startY - 50,0,80,10).attr({
                fill:"red",
                "fill-opacity":0.3,
                class:"rect"});

    var mini_y = startY/ratio - 40/ratio;
    var mini_rect = Snap("#full").rect((startX-offset)/ratio,mini_y,currentRect.attr("width")/ratio,80/ratio,2).attr({
                fill:"red",
                "fill-opacity":0.3,
                class:"rect",
                id : "mini-" + currentRect.id,
    });

    

    currentRect.click(clickOnRect);
}

var angle = function(xStart,xEnd,yStart,yEnd) {
    var x = xEnd - xStart;
    var y = yEnd - yStart;
    var hyp = Math.sqrt(x**2 + y**2);
    return Math.acos(x/hyp);
}

var manageDirection = function(event) {
    move = true; 
 
    event.preventDefault();
    x = event.changedTouches[0].pageX;
    y = event.changedTouches[0].pageY;

    var length = x - startX;

    //calcul de l'angle par rapport au point de départ 
    var ecart = angle(startX,x,startY,y);

    //si on va vers la droite et qu'on trace un trait "droit"
    //TODO : trouver une formule pour adapter la valeur à la taille de l'image 
    if (length> 0 && ecart < 0.09) {
        currentRect.attr({
            width:length
        })

        resizeMiniRect();
    }
}

var clickOnRect = function(event) {
    stopSelection();
    currentRect = this;
    var paper = this.paper;

    currentRect.attr({
        "fill-opacity":0.6
    })

    startX = currentRect.attr("x");
    startY = currentRect.attr("y");

    var x = parseInt(startX) + 10;
    var mid = parseInt(startY) + currentRect.attr("height")/2

    var leftArrow = paper.polyline(x,mid ,x+15,mid-15, x+15,mid+15).attr({
        fill:"#FFF",class:"arrow"});
    group.add(leftArrow);

    x = parseInt(startX) + parseInt(currentRect.attr("width")) - 10;

    var rightArrow  = paper.polyline(x,mid ,x-15,mid-15, x-15,mid+15).attr({
        fill:"#FFF",class:"arrow"});
    group.add(rightArrow);

    leftArrow.touchstart(resizeStart)
    rightArrow.touchstart(resizeStart)
    leftArrow.touchmove(moveLeftArrow)
    rightArrow.touchmove(moveRightArrow)
}

var resizeStart = function(event){
    event.preventDefault();
    arrowStartX = event.changedTouches[0].pageX
    arrowStartY = event.changedTouches[0].pageY
    currentWidth = currentRect.attr("width");
    currentX = currentRect.attr("x");
}

var moveLeftArrow = function(event) {
    event.preventDefault();
    var x = event.changedTouches[0].pageX
    var y = event.changedTouches[0].pageY
    var ecart = angle(arrowStartX,x,arrowStartY,y);

    if  (ecart < 0.09 || ecart > 2.2 ) {
        resizeRect(x,-1);
        resizeMiniRect();
        //move the origin of the rectangle
        var newX = currentX - (arrowStartX - x);
        currentRect.attr({
            x:newX
        })

       var points = this.attr("points");
       newX = parseInt(newX) + 10;
       var off = parseInt(newX) + 15
       this.attr({
           points: newX+","+points[1] +","+off+","+points[3]+","+off+","+points[5]
       })
    }
}

var moveRightArrow = function(event) {
    event.preventDefault();
    var x = event.changedTouches[0].pageX
    var y = event.changedTouches[0].pageY
    
    var ecart = angle(arrowStartX,x,arrowStartY,y);
    if  (ecart < 0.09 || ecart > 2.2 ) {
        resizeRect(x,1);
        resizeMiniRect();

        x = parseInt(startX) + parseInt(currentRect.attr("width")) - 10;
        var points = this.attr("points");
        var newX = parseInt(currentX) + parseInt(currentRect.attr("width")) - 10;
        var off = parseInt(newX) - 15
        this.attr({
           points: newX+","+points[1] +","+off+","+points[3]+","+off+","+points[5]
       })
    }
}

//orient == - 1 si on va à gauche et 1 si on va à droite
var resizeRect = function(x,orient) {
    var newWidth = currentWidth - ((arrowStartX - x)*orient)
    currentRect.attr({
        width:newWidth
    });
}

var resizeMiniRect = function() {
    var miniRect = Snap('#mini-' +currentRect.id);
    var offset = group.attr("transform").globalMatrix.e;
    miniRect.attr({
        x: parseInt(currentRect.attr("x")/ratio),
        width:parseInt(currentRect.attr("width")/ratio)
    })
}