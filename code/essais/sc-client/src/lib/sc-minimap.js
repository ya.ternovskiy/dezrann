/**
 * class managing a reduced view of an image
 */
export class MiniMap {

    /**
     * creates a mini map
     * @param {*} width
     * @param {*} height
     * @param {*} src source of the image displayed
     * @param {*} id
     * @param {*} ratio how much the size of the image has been reduced
     */
    constructor(width,height,src,id,ratio) {
        this.width = width/ratio;
        this.height = height/ratio;
        this.ratio = ratio;
        this.paper = Snap("#minimap").attr({
            height:this.height,
            width:this.width
        })

        this.group = this.paper.g()

        this.group.image(src,0,0,"100%","100%").attr({
            id:id
        })

        this.frame = new scFrame.Frame(this.paper,ratio)
    }

    /**
     * returns the frame associated to the mini map
     */
    getFrame() {
        return this.frame
    }

    //NOT USED

    drawMiniGlab(x,y,id,offset) {
        var mini_y = y/this.ratio - 40/this.ratio;
        this.group.rect((x-offset)/this.ratio,mini_y,0,80/this.ratio,2).attr({
            fill:"red",
            "fill-opacity":0.3,
            class:"rect",
            id:"mini-" +id
        })
    }

    resizeMiniGlab(id,x,width) {
        Snap('#mini-' +id).attr({
            x:parseInt(x/this.ratio),
            width:width/this.ratio
        })
    }
}
