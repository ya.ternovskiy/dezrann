/**
 * class managing the arrow used to move the glabs
 */
export class Arrow {

    /**
     * creates a new arrow
     * @param {*} x position x for the arrow
     * @param {*} y position y for the arrow
     * @param {*} offset width of the arrow, must be positive for a left arrow , negative for a right one
     * @param {*} fill color of the arrow
     * @param {*} id HTML id
     * @param {*} group svg group whcih the arrow will belong to
     * @param {*} glab svg rect for which the arrows has to be drawed
     */
    constructor(x,y,offset,fill,id,group,glab) {
        this.group = group;
        this.offset = offset;
        this.polyline = group.polyline(x,y,x+offset,y-15,x + offset,y+15).attr({
            fill:fill,
            class:'arrow',
            id:id
        })

        y = parseInt(glab.attr("y"));
        var h = parseInt(glab.attr("height"))
        this.zone = group.polyline(x-offset,y,x+offset,y,x + offset,y+h,x-offset,y+h).attr({
            class:'arrow',
            id:id,
            "fill-opacity":0,
        })
    }

    /**
     * moves the arrow
     * @param {*} x new position for the start of the arrow
     * @param {*} offset width of the arrow, must be positive for a left arrow , negative for a right one
     */
    move(x,offset) {
        var points = this.polyline.attr("points");
        this.polyline.attr({
            points: x + "," + points[1] + "," +offset + "," + points[3] + "," + offset + "," + points[5]
        });

        var points = this.zone.attr("points");
        this.zone.attr({
            points: parseInt(x-this.offset)+","+points[1]+","+parseInt(x+this.offset)+","+points[3]+","+parseInt(x+this.offset)+","+points[5]+","+parseInt(x-this.offset)+","+points[7]
        })
    }
}
