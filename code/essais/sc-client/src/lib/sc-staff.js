/**
 * class managing a score's staff
 */
export class Staff {

    /**
     * creates a staff
     * @param {*} top
     * @param {*} bottom
     */
    constructor(top,bottom) {
        this.top = top
        this.bottom = bottom
    }

    getTop() {
        return this.top;
    }

    getBottom() {
        return this.bottom;
    }

    getSize() {
        return this.bottom - this.top;
    }
}
