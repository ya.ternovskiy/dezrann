var gulp = require("gulp");
var babel = require("gulp-babel");
var sass = require("gulp-sass");
var liveserver = require("gulp-live-server");

gulp.task("js", function () {
    return gulp.src("src/js/*.js")
        .pipe(babel())
        .pipe(gulp.dest("build/js"));
})

gulp.task("css", function () {
    return gulp.src("src/sass/*.sass")
        .pipe(sass())
        .pipe(gulp.dest("build/css"));
})

gulp.task("default", function () {

    var server = liveserver.static(".", 3000);
    server.start();

    gulp.watch("src/js/*.js", ["js"])
    gulp.watch("src/sass/*.sass", ["css"])
})
