const onMIDIload = () => {
    
    MIDI.setVolume(0, 127)

    function play() {

        let note = parseInt(this.getAttribute("data-note"))

        MIDI.noteOn(0, note, 127, 0)
        MIDI.noteOff(0, note, 0.5)
    }

    let notes = document.querySelectorAll(".note")

    for (let i = 0; i < notes.length; i++) {

        notes.item(i).addEventListener("click", play)
    }

}

const setupEventListener = () => {

    let keyboardContainer = document.querySelector(".keyboard-container")

    let note = 41

    for (let i = 0; i < 35; i++) {

        if ([7, 13, 21, 27].indexOf(i) != -1) continue

        let t = document.createElement("div")

        let j = Math.floor(i / 2)

        if (i % 2 == 0) {

            t.style.left = `${j * 50.5}px`
            t.setAttribute("class", "note touche-blanche")
            t.setAttribute("data-note", note++)
        }
        else {

            t.style.left = `${j * 50.5 + 35}px`
            t.setAttribute("class", "note touche-noire")
            t.setAttribute("data-note", note++)
        }

        keyboardContainer.appendChild(t)
    }

    MIDI.loadPlugin({
        soundfontUrl: "bower_components/midi/examples/soundfont/",
        instrument: "acoustic_grand_piano",
        onprogress: function (state, progress) {
            console.log(state, progress)
        },
        onsuccess: onMIDIload
    })

}

window.addEventListener("load", setupEventListener)
