function select() {

    let sel = document.querySelector("#select-midi")
    let tempo = document.querySelector("#tempo")

    loadFile(sel.value, tempo.value)
}

function mute(i) {

    console.log(i)

    MIDI.channels[i].mute = !MIDI.channels[i].mute
    MIDI.Player.resume()
}

function setTime() {

    let time = document.querySelector("#time")
    MIDI.Player.currentTime = parseInt(time.value * 1000)

    if (MIDI.Player.playing)
        MIDI.Player.resume()
}

function update(time, end) {

    let cursor = document.querySelector(".cursor")
    let currentTime = document.querySelector(".current-time")

    let tm = Math.floor(MIDI.Player.currentTime / 1000)

    let mn = Math.floor(tm / 60)
    let sc = tm % 60

    currentTime.textContent = `${mn}:${sc < 10 ? '0' + sc : sc}`

    let progress = time / end * 100

    cursor.style.left = `${progress}%`
}

function loadFile(n, t) {

    if (t)
        MIDI.Player.BPM = t
    else
        MIDI.Player.BPM = 120

    MIDI.Player.loadFile(`midi/${n}.mid`, () => {

        console.log(MIDI)

        let endTime = document.querySelector(".end-time")
        let tm = Math.floor(MIDI.Player.endTime / 1000)
        let mn = Math.floor(tm / 60)
        let sc = tm % 60
        endTime.textContent = `${mn}:${sc < 10 ? '0' + sc : sc}`

        for (let i = 0; i < 15; i++)
            MIDI.channels[i].instrument = 0

        let timeline = document.querySelector(".timeline")

        timeline.addEventListener("click", (e) => {

            let timelineStyle = window.getComputedStyle(timeline)

            let time = MIDI.Player.endTime * (e.layerX / parseFloat(timelineStyle.width))

            MIDI.Player.currentTime = time

            if (MIDI.Player.playing)
                MIDI.Player.resume()
        })

        setInterval(() => {
            update(MIDI.Player.currentTime, MIDI.Player.endTime)
        }, 100)
    })
}

function setupEventListener() {

    MIDI.loadPlugin({
        soundfontUrl: "bower_components/midi/examples/soundfont/",
        instrument: "acoustic_grand_piano",
        onprogress: function onprogress(state, progress) {
            console.log(state, progress)
        },
        onsuccess: () => console.log("MIDI loaded")
    })
}

window.addEventListener("load", setupEventListener)