"use strict";

function select() {

    var sel = document.querySelector("#select-midi");
    var tempo = document.querySelector("#tempo");

    loadFile(sel.value, tempo.value);
}

function mute(i) {

    console.log(i);

    MIDI.channels[i].mute = !MIDI.channels[i].mute;
    MIDI.Player.resume();
}

function setTime() {

    var time = document.querySelector("#time");
    MIDI.Player.currentTime = parseInt(time.value * 1000);

    if (MIDI.Player.playing) MIDI.Player.resume();
}

function update(time, end) {

    var cursor = document.querySelector(".cursor");
    var currentTime = document.querySelector(".current-time");

    var tm = Math.floor(MIDI.Player.currentTime / 1000);

    var mn = Math.floor(tm / 60);
    var sc = tm % 60;

    currentTime.textContent = mn + ":" + (sc < 10 ? '0' + sc : sc);

    var progress = time / end * 100;

    cursor.style.left = progress + "%";
}

function loadFile(n, t) {

    if (t) MIDI.Player.BPM = t;else MIDI.Player.BPM = 120;

    MIDI.Player.loadFile("midi/" + n + ".mid", function () {

        console.log(MIDI);

        var endTime = document.querySelector(".end-time");
        var tm = Math.floor(MIDI.Player.endTime / 1000);
        var mn = Math.floor(tm / 60);
        var sc = tm % 60;
        endTime.textContent = mn + ":" + (sc < 10 ? '0' + sc : sc);

        for (var i = 0; i < 15; i++) {
            MIDI.channels[i].instrument = 0;
        }var timeline = document.querySelector(".timeline");

        timeline.addEventListener("click", function (e) {

            var timelineStyle = window.getComputedStyle(timeline);

            var time = MIDI.Player.endTime * (e.layerX / parseFloat(timelineStyle.width));

            MIDI.Player.currentTime = time;

            if (MIDI.Player.playing) MIDI.Player.resume();
        });

        setInterval(function () {
            update(MIDI.Player.currentTime, MIDI.Player.endTime);
        }, 100);
    });
}

function setupEventListener() {

    MIDI.loadPlugin({
        soundfontUrl: "bower_components/midi/examples/soundfont/",
        instrument: "acoustic_grand_piano",
        onprogress: function onprogress(state, progress) {
            console.log(state, progress);
        },
        onsuccess: function onsuccess() {
            return console.log("MIDI loaded");
        }
    });
}

window.addEventListener("load", setupEventListener);