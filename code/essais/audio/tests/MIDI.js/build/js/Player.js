"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * 
 */
var player = function () {

  /**
   * Permet d'initialiser une instance du type player à partir d'un
   * dictionnaire d'options.
   * 
   * @param   {Object} options
   */
  function player(options) {
    _classCallCheck(this, player);

    this.currentTime = options.currentTime;

    /* TODO */
  }

  /**
   * Charge une fichier MIDI dont la chemin d'accès est passé en paramètre.
   * 
   * @param   {String} path
   */


  _createClass(player, [{
    key: "load",
    value: function load(path) {}

    /* TODO */


    /**
     * Commence la lecture du fichier MIDI.
     */

  }, {
    key: "play",
    value: function play() {}

    /* TODO */


    /**
     * Stoppe la lecture du fichier MIDI.
     */

  }, {
    key: "stop",
    value: function stop() {}

    /* TODO */


    /**
     * Met en pause la lecture du fichier MIDI.
     */

  }, {
    key: "pause",
    value: function pause() {}

    /* TODO */


    /**
     * Rend muet la lecture du canal passé en paramètre.
     * 
     * @param   {Number} channel
     */

  }, {
    key: "mute",
    value: function mute(channel) {}

    /* TODO */


    /**
     * Rend muet tous les canaux ormis celui passé en paramètre.
     * 
     * @param   {Number} channel 
     */

  }, {
    key: "solo",
    value: function solo(channel) {}

    /* TODO */


    /**
     * Déplace le curseur courrant au temps passé en paramètre.
     * 
     * @param {String} time
     */

  }, {
    key: "setTime",
    value: function setTime(time) {

      /* TODO */
    }
  }]);

  return player;
}();