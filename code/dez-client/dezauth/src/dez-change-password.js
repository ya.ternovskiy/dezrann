/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-localstorage/iron-localstorage.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/app-route/app-location.js';
import '@polymer/iron-meta/iron-meta.js';
import './dez-global-variable.js';
import './dez-logout.js';
import './shared-styles.js';

class DezChangePassword extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        .wrapper-btns {
          margin-top: 15px;
        }
        paper-button.link {
            color: #757575;
        }
        .alert-error {
          background: #ffcdd2;
          border: 1px solid #f44336;
          border-radius: 3px;
          color: #333;
          font-size: 14px;
          padding: 10px;
        }
      </style>

      <iron-meta id="config" key="config" value="{{config}}"></iron-meta>

      <div class="card">
        <h1>Change password</h1>
        <template is="dom-if" if="[[error]]">
          <p class="alert-error"><strong>Error:</strong> [[error]]</p>
        </template>
        <paper-input label="Old Password" type="password" value="{{formData.oldpassword}}"></paper-input>
        <paper-input label="New Password" type="password" value="{{formData.newpassword}}"></paper-input>
        <paper-input label="Verify" type="password" value="{{formData.newpassword2}}"></paper-input>
        <div class="wrapper-btns">
          <paper-button raised class="primary" on-tap="postNewPassword">Change password</paper-button>
        </div>
      </div>

      <iron-ajax
          id="changepwAjax"
          method="post"
          content-type="application/json"
          handle-as="text"
          on-response="handleUserResponse"
          on-error="handleUserError">
      </iron-ajax>

      <iron-localstorage name="user-storage" value="{{storedUser}}"></iron-localstorage>
      <dez-global-variable key="userData" value="{{storedUser}}"></dez-global-variable>

      <app-location route="{{route}}"></app-location>

    `;
  }

  static get properties() {
    return {
      formData: {
        type: Object,
        value: {}
      },
      storedUser: Object,
      error: String
    }
  }

  postNewPassword() {
    this.$.changepwAjax.url = JSON.parse(this.config).dezauth + "/changepw";
    this.formData.uid = this.storedUser.name
    this.$.changepwAjax.body = this.formData;
    this.$.changepwAjax.generateRequest();
  }

  handleUserResponse(event) {
    var response = JSON.parse(event.detail.response);

    if (response.access_token) {
      this.error = '';
      this.storedUser = {
        name: this.formData.uid,
        access_token: response.access_token,
        loggedin: true
      };
      this.set('route.path', '/corpus');
    }

    // reset form data
    this.formData = {};
  }

  handleUserError(event) {
    this.error = event.detail.request.xhr.response;
  }

}

window.customElements.define('dez-change-password', DezChangePassword);
