class Label {

  constructor (jsonLabel) {
    this.id = jsonLabel.id
    this.type = jsonLabel.type
    this.tag = jsonLabel.tag
    this.comment = jsonLabel.comment
    this.start = jsonLabel.start
    this.duration = jsonLabel.duration
    this.staff = jsonLabel.staff
    this.line = jsonLabel.line
    this.color = jsonLabel.color
  }

  get end () { return this.start + this.duration; }
  set end (end) {
    if (end >= this.start) {
      this.duration = end - this.start;
    }
  }

  toString() {
    let s = ''
    s += this.id + '-(' + this.type + ','
    s += this.start
    // s += toMeasureFrac(this.start, this.timeSigNum, this.timeSigDenum, this.timeSigUpbeat) + '@' + this.start.toFixed(2) + ','
    s += this.tag + ')'
    return s
  }

}
