class LabelTypes {

    constructor () {
        this._types = {

            // Old types, for compatibility

            'structe': { 'color': '#ffff00', 'line': 'top.1', 'style': 'display:none' },
            'struct=': { 'color': '#ffff00', 'line': 'top.1', 'style': 'display:none'  },
            'ped': { 'color': '#335555', 'line': 'bot.1', 'style': 'display:none'  },
            'deg': { 'color': '#4400bb', 'line': 'bot.1', 'style': 'display:none'  },
            'h': { 'color': '#440000', 'line': 'bot.3', 'style': 'display:none'  },
            'seq': { 'color': '#ff8800', 'line': 'top.3', 'style': 'display:none'  },
            'seq:all': { 'color': '#ff8800', 'line': 'top.2', 'style': 'display:none'  },
            'seqnext': { 'color': '#ee7700', 'line': 'top.3', 'style': 'display:none'  },
            'seqnnext': { 'color': '#dd6600', 'line': 'top.3', 'style': 'display:none'  },
            'seqnnnext': { 'color': '#cc5500', 'line': 'top.3', 'style': 'display:none'  },
            'ext:top': { 'color': '#ff8800', 'line': 'bot.3', 'style': 'display:none'  },
            'ext:bot': { 'color': '#ff8800', 'line': 'bot.3', 'style': 'display:none'  },
            'pm': { 'color': '#22ff22', 'line': 'bot.3', 'style': 'display:none'  },

            // Displayed types (but not all)
            'S':   { 'color': '#ff0000' },
            'S-inv': { 'color': '#ff4444', 'style': 'display:none' },
            'S-inc': { 'color': '#ffaaaa', 'style': 'display:none' },

            'CS1': { 'color': '#00ff00' },
            'CS1a': { 'color': '#44ff44', 'style': 'display:none' },
            'CS1b': { 'color': '#22ff22', 'style': 'display:none' },
            'CS1-inc': { 'color': '#aaffaa', 'style': 'display:none' },

            'CS2': { 'color': '#0000ff' },
            'CS2a': { 'color': '#4444ff', 'style': 'display:none' },
            'CS2b': { 'color': '#2222ff', 'style': 'display:none' },
            'CS2-inc': { 'color': '#aaaaff', 'style': 'display:none' },

            'S2':  { 'color': '#aa6600', 'style': 'display:none' },
            'S3':  { 'color': '#aa0066', 'style': 'display:none' },

            'Pattern': { 'color': '#aa3300', 'line': 'bot.2', 'style': 'margin-top: 15px' },
            'Theme':   { 'color': '#aa0033', 'line': 'bot.2', 'style': 'margin-top: 15px'  },

            'Tonality':   { 'color': '#440088', 'line': 'bot.2', 'style': 'margin-top: 20px'  },
            'Modulation': { 'color': '#4400ee', 'line': 'bot.2' },
            'Harmony':    { 'color': '#4400bb', 'line': 'bot.1' },

            'Pedal':   { 'color': '#335555', 'line': 'bot.1', 'style': 'margin-top: 15px'  },
            'Cadence': { 'color': '#ff00ff', 'line': 'all' },

            'Harmonic sequence': { 'color': '#ff8800', 'line': 'bot.3', 'style': 'margin-top: 15px'  },
            'Texture': { 'color': '#22ff22', 'line': 'bot.3' }, // Unison, Homorythmy, Parallel Move

            'Structure': { 'color': '#ffff00', 'line': 'top.1', 'style': 'margin-top: 15px'  },

            'Comment': { 'color': '#dddddd', 'line': 'bot.3', 'style': 'margin-top: 15px; font-style:italic'  },
            'Edmus': { 'color': '#dddddd', 'style': 'display:none', 'tags' : {} }
        };

        this._colors = [
          '255, 0, 0',    //'red',
          '255, 255, 0',  //'yellow',
          '0, 128, 0',    //'green',
          '0, 0, 255',    //'blue',
          '0, 255, 255',  //'aqua',
          '75, 0, 130',   //'indigo',
          '255, 192, 203',//'pink',
          '128, 0, 0',    //'maroon',
          '128, 0, 128',  //'purple',
          '255, 0, 255',  //'fuchsia',
          '0, 255, 0',    //'lime',
          '0, 0, 128',    //'navy',
          '128, 128, 0',  //'olive',
          '255, 165, 0',  //'orange',
          '0, 128, 128'   //'teal'
        ]

        this._freeColorIndex = 0
        this._usedTags = []
        this._neededTags = []
    }

    formatLabel (label) {

    }

    set usedTags (tags) {
      this._usedTags = tags
    }

    set neededTags (tags) {
      this._neededTags = tags
    }

    _freeColors (type) {
      if (Object.keys(this._types[type].tags).length === 0) return
      let tags = this._neededTags.concat(this._usedTags)
      let tagsColors = {}
      tags.forEach(tag => {
        tagsColors[tag] = this._types[type].tags[tag]
      })
      this._types[type].tags = tagsColors
      this._setFreeColorIndex(type)
    }

    _setFreeColorIndex (type) {
      this._freeColorIndex = 0
      let tags = this._neededTags.concat(this._usedTags)
      let tagsOfType = this._types[type].tags
      let usedColors = Object.keys(tagsOfType)
      .filter(tag => tagsOfType[tag] != undefined)
      .map(tag => tagsOfType[tag].color )
      for (let i = 0; i < this._colors.length; i++) {
        let color = this._colors[i]
        if (!usedColors.includes(color)) {
          this._freeColorIndex = i
          break
        }
      }
    }

    colorOf (type) {
        return (this._types[type] && this._types[type].color) || "#000";
    }

    colorOfType(typeTag) {
      let type = typeTag.type
      let tag = typeTag.tag
      if (tag == undefined || tag == "") {
        return new Color(this.colorOf(type))
      }
      if (this._types[type]) {
        if (this._types[type].tags) {
          if (this._types[type].tags[tag]) {
            return new Color(this._types[type].tags[tag].color)
          } else {
            this._freeColors(type)
            let ret = this._colors[this._freeColorIndex]
            this._types[type].tags[tag]={'color': ret}
            return new Color(ret)
          }
        } else {
          return new Color(this.colorOf(type))
        }
      } else {
        return new Color("#000")
      }
    }

    coloredTag (tag, type) {
      return {
        name: tag,
        color: this.colorOfType({ type: type, tag: tag })
      }
    }

    setColorOfType (typeTag, color) {
      if (typeTag.type != undefined) {
        if (this._types[typeTag.type].tags == undefined) {
          this._types[typeTag.type].tags = {}
        }
        if (this._types[typeTag.type].tags[typeTag.tag] == undefined) {
          this._types[typeTag.type].tags[typeTag.tag] = {}
        }
        this._types[typeTag.type].tags[typeTag.tag].color = (new Color(color)).rgb
      }
    }

    get types () {
        return Object.keys(this._types);
    }

    get typesNameColor () {
        return Object.keys(this._types).map(name => {
          let color = new Color(this._types[name].color)
          return {
            'name': name,
            'color': this._types[name].color,
            'rgb': color.rgb,
            'style': this._types[name].style
          }
        });
    }

    knownType (type) {
        return Object.keys(this._types).includes(type);
    }

    defaultLineOf (type) {
        return (this._types[type] && this._types[type].line) || "bot.3";
    }

}

class Color {

  constructor (color){
    let rgbFormat = /[0-9]{1,3},[ ]*[0-9]{1,3},[ ]*[0-9]{1,3}/g
    let hexFormat = /#[0-9a-fA-F]{3,6}/g
    if (color.match(hexFormat)) {
      this._hex = color
      this._rgb = this._hexaToRgb(color)
    } else if (color.match(rgbFormat)) {
      this._hex = this._rgbToHexa(color)
      this._rgb = color
    } else {
      this._hex = "#000"
      this._rgb = "0, 0, 0"
    }
  }

  get hex () { return this._hex }
  get rgb () { return this._rgb }

  _hexaToRgb (hexa) {
    let hex = hexa.replace('#','');
    let r = parseInt(hex.substring(0,2), 16);
    let g = parseInt(hex.substring(2,4), 16);
    let b = parseInt(hex.substring(4,6), 16);
    return r+', '+g+', '+b;
  }

  _compToHex(comp) {
    var hex = Number(comp).toString(16);
    if (hex.length < 2) {
         hex = "0" + hex;
    }
    return hex;
  }

  _rgbToHexa(rgb) {
    let trgb = rgb.split(',')
    return "#"
    + this._compToHex(parseInt(trgb[0]))
    + this._compToHex(parseInt(trgb[1]))
    + this._compToHex(parseInt(trgb[2]))
  }

}
