# \<dez-canvas\>

Dezrann canvas

## Install dependencies

First install [Node.js](https://nodejs.org/).

Install brower:

```
(sudo) sudo npm install -g bower
```

Install dependencies:

```
bower install
```

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your element locally.

## Viewing Your Element

```
$ polymer serve
```

Open a browser and go to `http://localhost:8081/components/dez-canvas/demo/` (if
`polymer server` to 8081).

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.

To run component tests in a browser go to `http://localhost:8081/components/dez-canvas/test/`.

To run unit test in a browser got to `http://localhost:8081/components/dez-canvas/test/unit.html`.
