class SnapText {

    constructor (textView, marginTop) {
        this._textView = textView;
        this._marginTop = marginTop;
    }

    get text () {
        // when empty, snap.paper.text is an empy array ([]) instead of ""...
        let text = this._textView.attr("text")
        return text.length == 0 ? "" : text;
    }
    set text (text) {
        this._textView.attr({"text": text});
    }

    moveTo (x, y) {
        this._textView.attr({"x": x, "y": y + this._marginTop})
    }

    get box () {
        if (this._box == undefined) {
            this._box = new Box(this._textView)
        }
        return this._box;
    }

    remove () {
        this._textView.remove();
    }

    hide () {
        this.moveTo(-100,0)
    }

}

class Box {

    constructor (text) {
        let bbox = text.getBBox()
        this._width = bbox.x2 - bbox.x
        this._height = bbox.y2 - bbox.y
    }

    get width () { return this._width }
    get height () { return this._height }

}
