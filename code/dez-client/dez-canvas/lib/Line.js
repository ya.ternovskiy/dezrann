class Line {

    constructor (top, bottom) {
        if (top < 0 || bottom < 0) throw "top or bottom must be positive"
        if (top == bottom) throw "top and bottom must be different"
        if (top > bottom) throw "top must be loawer than bottom!"
        this._top = top
        this._bottom = bottom
        this._grobs = [];
        this._parent;
    }

    get top () { return this._top}
    get bottom () { return this._bottom}
    set top (top) { this._top = top }
    set bottom (bottom) { this._bottom = bottom}
    set parent (parent) { this._parent = parent }

    get id () { return this._id }
    set id (id) { this._id = id }

    shift (height) {
        this._top = this._top + height;
        this._bottom = this._bottom + height;
    }

    equalTo (line) {
        return this.top == line.top && this.bottom == line.bottom && this.group == line.group
    }

    on (y) {
        return this.top <= y && y <= this.bottom
    }

    in (y1, y2) {
        if (y1 < y2) {
            return this.top <= y2 && this.bottom >= y1
        } else {
            return this.top <= y1 && this.bottom >= y2
        }
    }

    /* used by parent */
    between (top) {
        this._betweenTop = top
        return this
    }

    and (bottom) {
        return this.top > this._betweenTop && this.bottom < bottom
    }

    _has(grob) {
        return this._grobs.filter(item => item == grob).length > 0
    }

    add (grob) {
        if (this._has(grob)) return;
        grob.addParent(this)
        grob.show();
        this._grobs.push(grob)
        this._parent && this._parent
            .getChildrenFrom(grob.top)
            .to(grob.bottom)
            .map(sister => sister.add(grob))
    }

    delete (grob) {
        if (!this._has(grob)) return;
        grob.deleteParent(this)
        this._grobs = this._grobs.filter(item => item != grob)
        grob.show()
    }

    grobOn(x) {
        return this._grobs.filter(grob => grob.contains(x))[0]
    }

    grobsOn(x) {
        return this._grobs.filter(grob => grob.contains(x))
    }

    get empty () { return this._grobs.length == 0 }

}
