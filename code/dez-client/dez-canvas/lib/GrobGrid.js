/**
 * Class representing the positions where grobs can be placed in the canvas: to
 * start or to end. It is used by all grobs and only one instance exists for one
 * canvas.
 */
class GrobGrid {

    /**
     * If the array of positions is null or undefined, it is replaced by an empty
     * array. An exception is thrown if there are negative values in the array.
     * Positions are finally sorted.
     * @param  {array} positions array of positions (float)
     */
    constructor (positions) {
        this.positions = positions;
    }

    set positions (positions) {
        this._positions = positions?positions:[]
        if (this._positions.filter(pos => pos < 0).length > 0) {
            throw "Positions can't be negative"
        }
        this._positions.sort((a,b)=>a-b)
    }

    /**
     * Return the closest position by default to the given one.
     * @private
     * @param  {float} x given position
     * @return {float}   closest position by default
     */
    _defaultClosest (x) {
        return this._positions.reduce(
            (closest, pos) => {
                if (x >= pos) return pos
                return closest
            },
            this._positions[0]
        );
    }

    /**
     * Return the closest position by excess to the given one.
     * @private
     * @param  {float} x given position
     * @return {float}   closest position by excess
     */
    _excessClosest (x) {
        return this._positions.reduceRight(
            (closest, pos) => {
                if (x <= pos) return pos
                return closest
            },
            this._positions.slice(-1)[0]
        );
    }

    /**
     * Convert a given position according to the grid positions. The closest by
     * default or by excess is returned. It returns the identity if there is no
     * positions.
     * @public
     * @param  {float} x position to convert according to the grid positions
     * @return {float}   closest position to the given one
     */
    convert (x) {
        if (this._positions.length == 0) return x;
        let defaultClosest = this._defaultClosest(x);
        let excessClosest = this._excessClosest(x);
        let middle = (excessClosest + defaultClosest)/2
        return x<=middle?defaultClosest:excessClosest;
    }

}
