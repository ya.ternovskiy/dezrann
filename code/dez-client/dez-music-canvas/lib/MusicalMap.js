function round2(x) {
    return Math.round(x * 100) / 100;
}

function findDicho(element, slice, from, to) {
    let first = 0;
    let last = slice.length - 1;
    if (element <= slice[first][from]) {
        return slice[first][to]
    }
    if (element >= slice[last][from]) {
        return slice[last][to]
    }
    let index = Math.round((last-first)/2)
    let current = slice[index]
    let previous = slice[index-1]
    if (previous[from] <= element && element <= current[from]) {
        let alpha = (element - previous[from]) / (current[from] - previous[from])
        return round2(previous[to] + (current[to] - previous[to]) * alpha)
    } else if (element > current[from]) {
        return findDicho(element, slice.slice(index, last+1), from, to)
    } else if (element < previous[from]) {
        return findDicho(element, slice.slice(first, index), from, to)
    }
}


class MusicalMap {
    constructor () {
        this._positions = [];
        this._timeByMeasure = 4
        this._divByTime = 4
    }
    setPositions (positions) {
        this._positions = positions;
    }

    toMusicalOnset (x) {
        return findDicho(
            x,
            this._positions,
            "x",
            "onset"
        )
    }
    toMusicalDuration (startx, length) {
        return this.toMusicalOnset(startx+length) - this.toMusicalOnset(startx)
    }
    toGraphicalX(onset) {
        let x = findDicho(
            onset,
            this._positions,
            "onset",
            "x"
        )
        return x;
    }
    toGraphicalLength (start, duration) {
        let begin, end;
        begin = this.toGraphicalX(start);
        end = this.toGraphicalX(start+duration);
        return end - begin;
    }

}

class WaveMap extends MusicalMap {

   constructor () {
        super();
        this._positions = [];
        this._synchro = [];
        this._timeByMeasure = 4
        this._divByTime = 4
    }
    setSynchro (synchro) {
        this._synchro = synchro
    }

    toMusicalOnset (x) {
        let date = findDicho(
            x,
            this._positions,
            "x",
            "date"
        )

        return findDicho(
            date,
            this._synchro,
            "date",
            "onset"
        )
    }

    toGraphicalX(onset) {
        let date = findDicho(
            onset,
            this._synchro,
            "onset",
            "date"
        )

        let x = findDicho(
            date,
            this._positions,
            "date",
            "x",
        )
        return x;
    }

    regularGraphicalGrid (step, upbeat) {
        let ret = []
        let slice = this._synchro.slice(-1);
        let dernier = slice[0];
        for (let i = upbeat; i < dernier.onset; i+=step) {
            ret.push(this.toGraphicalX(i));
        }
        return ret;
    }


}

class Synchro {

  constructor (synchroData) {
    this._synchroData = synchroData
  }

  toOnset (date) {
    return findDicho(
        date,
        this._synchroData,
        "date",
        "onset"
    )
  }

  toDate (onset) {
    return findDicho(
      onset,
      this._synchroData,
      "onset",
      "date"
    )
  }

}
