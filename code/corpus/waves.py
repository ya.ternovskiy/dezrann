
import numpy as np
from scipy import signal
import pydub
from PIL import Image, ImageDraw

import math
import sys
import os
import tools

h_MARGIN = 30

DIR_OUT = './tmp/'

MP3_TO_CBR = "ffmpeg -y -i %s -codec:a libmp3lame -b:a 192k %s"
MP3_TO_OGG = "ffmpeg -y -i %s -codec:a libvorbis -q:a 4 %s"

# Inspired from https://gist.github.com/moeseth/130cd92dc47c56c47030

def to255(x, light=0, inverse=False):
    if inverse:
        x = 1 - x
    xx = light + x * (1 - light)
    return min(max(int(xx * 256), 0), 255)

def audio_to_bars(data, nb=1000):
    ticks = len(data)
    ticksb = ticks/nb
    print('    bars: %d, ticks: %d, ticks/bar: %d' % (nb, ticks, ticksb))
    
    bars = []

    for i in range(nb):
        span = data[int(i*ticksb):int((i+1)*ticksb)]
        try:
            max_abs = max(map(abs, span))
        except:
            print('!', i, span)
            max_abs = 0
        # avg = sum(span) / length(span)    
        # print(span, max_abs, avg)
        bars.append(max_abs)
        if (i % 100) == 0:
            sys.stdout.write('.')

    print()
    
    return bars



def audio_to_spectro_img(data, f):
    fr, t, spec = signal.spectrogram(data, 100.0, nperseg=2048)
    nf = len(fr)
    nt = len(t)

    jm = int(nf/3)
    im = Image.new('RGBA', (nt, jm), (255, 255, 255, 1))
    pixels = im.load()

    print("Spectogram %d x %d..." % (nt, nf))
    for j, row in enumerate(spec):
        if j == jm:
            break
        ma =  max(row)
        avg = sum(row) / len(row)
        for i, x in enumerate(row):
            if x <= 0:
                continue
            l = math.log(x, 10)
            v1 = to255(x / (2*avg), light=.6, inverse=True)
            v2 = to255(abs(l) - int(abs(l)), light=.7, inverse=True)
            v3 = to255(abs(l/3) - int(abs(l/3)), light=.8, inverse=True)
            pixels[i,jm-j-1] = (v2, v1, v3)

    print('==>', f, im)
    im.save(f)
    return nt


def soft_bars(bars, alpha = .7):
    print('    Softing bars')
    bars_soft = []
    last_b = 0
    for b in bars + [0]:
        b_soft_1 = last_b * alpha + b * (1-alpha)
        b_soft_2 = last_b * (1-alpha) + b * alpha
        bars_soft += [b_soft_1, b_soft_2, b]
        last_b = b
    return bars_soft

def bars_to_img(bars, f, bar_h=300, bar_w=1, expand=1.3):
    h_ratio = bar_h / max(bars) #* expand

    im_w = len(bars) * bar_w
    im_h = bar_h + h_MARGIN * 2
    im = Image.new('RGBA', (im_w, im_h), (255, 255, 255, 1))
    draw = ImageDraw.Draw(im)

    y_mid = im_h / 2
    x = 0
    for bar in bars:
        h = bar * h_ratio
        draw.line((x, y_mid - h/2, x, y_mid + h/2), fill=(150, 150, 150), width=bar_w)
        draw.line((x, y_mid - h/4, x, y_mid + h/4), fill=(100, 100, 100), width=bar_w)
        x += bar_w

    draw.line((0, y_mid, im_w, y_mid), fill=(50, 50, 50))
    print('==>', f, im)
    im.save(f)

    return im_w



def parseSynchroCSV(f):
    # 97.967437641,38.2
    synchro = []
    print('<== ', f)
    prev_onset = 0
    shift = 0
    mult = 1
    for l in open(f):        
        try:
            if l.startswith('! shift'):
                shift = int(l.replace('! shift', '').strip())
                print('! shift %s' % shift)
                continue

            if l.startswith('! mult'):
                mult = float(l.replace('! mult', '').strip())
                print('! mult %s' % mult)
                continue
 
            
            ll = l.split(',')
            date = float(ll[0])
            mesbeat = 1 + float(ll[1])

            beat = int((mesbeat - int(mesbeat)) * 10 - 1 + .1)
            onset = ((int(mesbeat - 1) - 1) * 4 + beat) * mult + shift

            if onset - prev_onset > 20:
                print('! Skipping stange synchro event', beat, onset, ll[1].strip())
                continue
            prev_onset = onset
            
            synchro += [{ 'onset': onset, 'date': date }]
        except:
            print('! not parsed: ', l)
    print('     %d synchro events' % len(synchro))
    return synchro



def process(outdir, PIECE_INDEX, f_audio, score = None, generate_sp = False):
    print('<==', f_audio)
    audio = pydub.AudioSegment.from_file(f_audio)
    duration_in_seconds = len(audio)/1000
    data = np.fromstring(audio._data, np.int16)

    os.system('mkdir -p %s' % outdir)

    # Spectogram
    if generate_sp:
        f_spec = '%s-spec.png' % PIECE_INDEX
        f_spec_pos = '%s-spec-pos.json' % PIECE_INDEX
        w_spec = audio_to_spectro_img(data, outdir + '/' + f_spec)

    # Waveform
    WAVE_HEIGHT = 300
    IMAGE_HEIGHT = WAVE_HEIGHT + 2 * h_MARGIN
    f_waves = '%s-waves.png' % PIECE_INDEX
    b = audio_to_bars(data)
    bb = soft_bars(b)
    width = bars_to_img(bb, outdir + '/' + f_waves, bar_h=WAVE_HEIGHT)

    f_audio_cp = '%s.mp3' % PIECE_INDEX
    f_pos = '%s-waves-pos.json' % PIECE_INDEX
    f_sync = 'synchro.json' # % PIECE_INDEX


    pos = {}

    if score:
        nb = len(score['staffs'])
        last_onset = score['onset-x'][-1]['onset']
    else:
        nb = 1
        last_onset = 99

    pos['staffs'] = [ { 'top': i*IMAGE_HEIGHT/nb + 5, 'bottom': (i+1)*IMAGE_HEIGHT/nb - 5 } for i in range(nb) ]
    pos['date-x'] = [ { 'x': 0, 'date': 0 }, { 'x': width, 'date': duration_in_seconds } ]

    sync = {}
    sync['onset-date'] = [ { 'onset': 0, 'date': 0 }, { 'onset': last_onset, 'date': duration_in_seconds } ]


    try:
        f_synchro = f_audio.replace('.mp3', '.csv')
        synchro_data = parseSynchroCSV(f_synchro)
        sync['onset-date'] = synchro_data
        pos['date-x'][-1]['date'] = synchro_data[-1]['date']
    except Exception as e:
        print('! Error in parsing synchro data', e)

    # Temp
    sync = sync['onset-date']
        
    tools.write_json(pos, outdir + '/' + f_pos)
    tools.write_json(sync, outdir + '/' + f_sync)

    os.system(MP3_TO_CBR % (f_audio, outdir + '/' + f_audio_cp))

    audio = {
        "file": f_audio_cp,
        "onset-date": f_sync,
        "images" : [
            {'type': "wave", 'image': f_waves, 'positions': f_pos }
        ]
    }

    # Spectogram positions
    if generate_sp:
        pos_spec = pos
        pos_spec['date-x'][-1]['x'] = w_spec
        audio['images'] += [ {'type': "wave", 'name': "sp", 'image': f_spec, 'positions': f_spec_pos } ]
        tools.write_json(pos_spec, outdir + '/' + f_spec_pos)

    return audio


if __name__ == '__main__':

    id = sys.argv[1]
    f_audio = sys.argv[2]
    
    json = process(DIR_OUT, id, f_audio)
    print(json)
    
    # b = audio_to_bars(sys.argv[1], 4000) # './wtc-i-02.mp3'
    # bb = soft_bars(b)
    # bars_to_img(bb, 'w.png')



