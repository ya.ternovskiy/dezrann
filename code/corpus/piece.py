#!env python3
# coding: utf8

# python3 piece.py data/bach-fugues/bwv847.raw.ly [...]
# python3 piece.py data/edmus/bla/bla.mp3 [...]

import json
import sys
import os.path
import music
import lilypond
import waves
import tools

def process(f_in, f_lily, PIECE_INDEX, infos={}, f_audio = None, run_lilypond=True):
    print('--- Processing %s' % f_in)
    
    # Prepares 'info.json'
    infos['id'] = PIECE_INDEX

    for key, default_value in [
            ('title', PIECE_INDEX),
            ('composer', ''),
            ('time-signature', '4/4')
    ]:
        if not key in infos:
            infos[key] = default_value

    infos['sources'] = { "images": [], "audios": [] }

    # Prepares lilypond
    if f_in:
        ok = music.music_to_lilypond(f_in, f_lily, infos)
        if not ok:
            return False

    # Create output dir
    outdir = "../../corpus/" + PIECE_INDEX
    outdir_dez = outdir + "/analyses/"
    os.system('mkdir -p %s' % outdir_dez)
    
    # Process lilypond
    if f_lily:
        f_pos, score = lilypond.process(outdir, PIECE_INDEX, f_lily, infos, run_lilypond)
        infos['sources']['images'].append({'type': "score", 'image': "%s.png" % PIECE_INDEX, 'positions': f_pos})
    else:
        infos['sources']['images'].append({'type': "score", 'image': "../bwv846/bwv846.png", 'positions': "../bwv846/positions.png"})
        score = {}

    # Process waveform
    if f_audio:
      try:
          audio = waves.process(outdir, PIECE_INDEX, f_audio, score)
          infos['sources']['audios'].append(audio)
      except Exception as e:
        print('! Error in processing audio file:', e)

    # Write 'info.json'
    tools.write_json(infos, outdir + "/info.json")
    
    # Write 'empty.dez'
    empty = {}
    empty['labels'] = []
    tools.write_json(empty, outdir_dez + "/empty.dez")

    return True
    
if __name__ == '__main__':

    for f in sys.argv[1:]:

        if 'ly' in f:
            PIECE_INDEX = os.path.basename(f).replace('.raw.ly', '').replace('.mp3', '')
            process(None, f, PIECE_INDEX)
        else:
            # only audio file
            PIECE_INDEX = os.path.basename(f).replace('.mp3', '')
            process(None, None, PIECE_INDEX, f_audio = f)
