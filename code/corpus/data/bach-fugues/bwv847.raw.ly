\version "2.19" 
\include "lilypond-book-preamble.ly"
    
color = #(define-music-function (parser location color) (string?) #{
        \once \override NoteHead #'color = #(x11-color color)
        \once \override Stem #'color = #(x11-color color)
        \once \override Rest #'color = #(x11-color color)
        \once \override Beam #'color = #(x11-color color)
     #})
    
\header { 
 title = "Fuga 2, Vol. 1"   
  subtitle = "Das wohltemperirte Clavier"   
  
  } 
 
\score  { 
 
      << \new Staff  = spinery { \clef "treble" 
             \key ees \major 
             \time 4/4
             \key c \minor 
             r 1  
             \bar "|"  %{ end measure 1 %} 
             r 1  
             \bar "|"  %{ end measure 2 %} 
             r 8  
             g'' 16  
             fis'' 16  
             g'' 8  
             c'' 8  
             ees'' 8  
             g'' 16  
             fis'' 16  
             g'' 8  
             a'' 8  
             \bar "|"  %{ end measure 3 %} 
             d'' 8  
             g'' 16  
             fis'' 16  
             g'' 8  
             a'' 8  
             c'' 16  
             d'' 16  
             ees'' 4  
             d'' 16  
             c'' 16  
             \bar "|"  %{ end measure 4 %} 
             bes' 8  
             ees'' 16  
             d'' 16  
             ees'' 8  
             g' 8  
             aes' 8  
             f'' 16  
             ees'' 16  
             f'' 8  
             a' 8  
             \bar "|"  %{ end measure 5 %} 
             bes' 8  
             g'' 16  
             f'' 16  
             g'' 8  
             b' 8  
             c'' 8  
             d'' 16  
             ees'' 16  
             f'' 4  ~  
             \bar "|"  %{ end measure 6 %} 
             f'' 8  
             ees'' 16  
             d'' 16  
             c'' 16  
             bes' 16  
             aes' 16  
             g' 16  
             f' 8  
             aes'' 8  
             g'' 8  
             f'' 8  
             \bar "|"  %{ end measure 7 %} 
             ees'' 8  
             d'' 8  
             ees'' 8  
             f'' 8  
             b' 8  
             c'' 8  
             d'' 8  
             b' 8  
             \bar "|"  %{ end measure 8 %} 
             c'' 8  
             g'' 16  
             fis'' 16  
             g'' 8  
             d'' 8  
             ees'' 4  
             r 8  
             e'' 8  
             \bar "|"  %{ end measure 9 %} 
             f'' 8  
             f'' 16  
             e'' 16  
             f'' 8  
             c'' 8  
             d'' 4  
             r 8  
             d'' 8  
             \bar "|"  %{ end measure 10 %} 
             ees'' 8  
             ees'' 16  
             d'' 16  
             ees'' 8  
             bes' 8  
             c'' 8  
             ees'' 16  
             d'' 16  
             ees'' 8  
             f'' 8  
             \bar "|"  %{ end measure 11 %} 
             bes' 8  
             ees'' 16  
             d'' 16  
             ees'' 8  
             f'' 8  
             aes' 16  
             bes' 16  
             c'' 4  
             bes' 16  
             aes' 16  
             \bar "|"  %{ end measure 12 %} 
             g' 16  
             ees' 16  
             f' 16  
             g' 16  
             aes' 16  
             bes' 16  
             c'' 16  
             d'' 16  
             ees'' 16  
             d'' 16  
             c'' 16  
             d'' 16  
             ees'' 16  
             f'' 16  
             g'' 16  
             a'' 16  
             \bar "|"  %{ end measure 13 %} 
             bes'' 16  
             f' 16  
             g' 16  
             aes' 16  
             bes' 16  
             c'' 16  
             d'' 16  
             e'' 16  
             f'' 16  
             ees'' 16  
             d'' 16  
             ees'' 16  
             f'' 16  
             g'' 16  
             a'' 16  
             b'' 16  
             \bar "|"  %{ end measure 14 %} 
             c''' 8  
             b'' 16  
             a'' 16  
             g'' 16  
             f'' 16  
             ees'' 16  
             d'' 16  
             c'' 8  
             ees'' 8  
             d'' 8  
             c'' 8  
             \bar "|"  %{ end measure 15 %} 
             bes' 8  
             a' 8  
             bes' 8  
             c'' 8  
             fis' 8  
             g' 8  
             a' 8  
             fis' 8  
             \bar "|"  %{ end measure 16 %} 
             g' 8  
             d'' 16  
             c'' 16  
             d'' 8  
             r 8  
             r 8  
             e'' 16  
             d'' 16  
             e'' 8  
             r 8  
             \bar "|"  %{ end measure 17 %} 
             r 8  
             fis'' 16  
             e'' 16  
             fis'' 8  
             r 8  
             r 8  
             g' 16  
             f' 16  
             g' 8  
             r 8  
             \bar "|"  %{ end measure 18 %} 
             r 8  
             a' 16  
             g' 16  
             a' 8  
             r 8  
             r 8  
             b' 16  
             a' 16  
             b' 8  
             r 8  
             \bar "|"  %{ end measure 19 %} 
             r 8  
             c'' 16  
             b' 16  
             c'' 8  
             g' 8  
             aes' 8  
             c'' 16  
             b' 16  
             c'' 8  
             d'' 8  
             \bar "|"  %{ end measure 20 %} 
             g' 8  
             c'' 16  
             b' 16  
             c'' 8  
             d'' 8  
             f' 16  
             g' 16  
             aes' 4  
             g' 16  
             f' 16  
             \bar "|"  %{ end measure 21 %} 
             ees' 8  
             c'' 16  
             b' 16  
             c'' 8  
             g' 8  
             aes' 4  
             r 8  
             a' 8  
             \bar "|"  %{ end measure 22 %} 
             bes' 8  
             bes' 16  
             a' 16  
             bes' 8  
             f' 8  
             g' 4  
             r 8  
             g' 8  ~  
             \bar "|"  %{ end measure 23 %} 
             g' 8  
             aes' 16  
             bes' 16  
             c'' 16  
             b' 16  
             c'' 16  
             aes' 16  
             f' 2  ~  
             \bar "|"  %{ end measure 24 %} 
             f' 8  
             d'' 16  
             c'' 16  
             d'' 8  
             f' 8  
             ees' 8  
             ees'' 16  
             d'' 16  
             ees'' 8  
             g' 8  
             \bar "|"  %{ end measure 25 %} 
             f' 8  
             f'' 16  
             ees'' 16  
             f'' 8  
             aes' 8  
             g' 16  
             f'' 16  
             ees'' 16  
             d'' 16  
             c'' 16  
             b' 16  
             a' 16  
             g' 16  
             \bar "|"  %{ end measure 26 %} 
             c'' 8  
             f'' 8  
             ees'' 8  
             d'' 8  
             r 8  
             aes' 8  
             g' 8  
             f' 8  
             \bar "|"  %{ end measure 27 %} 
             g' 8  
             f' 16  
             ees' 16  
             f' 8  
             d' 8  
             aes' 8  
             g' 8  
             r 8  
             a' 8  
             \bar "|"  %{ end measure 28 %} 
             b' 8  
             c'' 8  
             f' 16  
             ees' 16  
             d' 16  
             c' 16  
             c' 8  
             c'' 16  
             b' 16  
             c'' 8  
             g' 8  
             \bar "|"  %{ end measure 29 %} 
             aes' 8  
             c'' 16  
             b' 16  
             c'' 8  
             < b'  d''  > 8   
             g' 8  
             c'' 16  
             b' 16  
             c'' 8  
             d'' 8  
             \bar "|"  %{ end measure 30 %} 
             f' 16  
             g' 16  
             aes' 4  
             g' 16  
             f' 16  
             e' 2  
             \bar "||"  %{ end measure 31 %} 
              } 
            
 
       \new Staff  = spinerx { \clef "treble" 
             \key ees \major 
             \time 4/4
             \key c \minor 
             r 8  
             c'' 16  
             b' 16  
             c'' 8  
             g' 8  
             aes' 8  
             c'' 16  
             b' 16  
             c'' 8  
             d'' 8  
             \bar "|"  %{ end measure 1 %} 
             g' 8  
             c'' 16  
             b' 16  
             c'' 8  
             d'' 8  
             f' 16  
             g' 16  
             aes' 4  
             g' 16  
             f' 16  
             \bar "|"  %{ end measure 2 %} 
             ees' 16  
             c'' 16  
             b' 16  
             a' 16  
             g' 16  
             f' 16  
             ees' 16  
             d' 16  
             c' 8  
             ees'' 8  
             d'' 8  
             c'' 8  
             \bar "|"  %{ end measure 3 %} 
             bes' 8  
             a' 8  
             bes' 8  
             c'' 8  
             fis' 8  
             g' 8  
             a' 8  
             fis' 8  
             \bar "|"  %{ end measure 4 %} 
             g' 4  
             r 16  
             c' 16  
             d' 16  
             ees' 16  
             f' 16  
             g' 16  
             aes' 8  ~  
             aes' 16  
             d' 16  
             ees' 16  
             f' 16  
             \bar "|"  %{ end measure 5 %} 
             g' 16  
             a' 16  
             bes' 8  ~  
             bes' 16  
             ees' 16  
             f' 16  
             g' 16  
             aes' 16  
             g' 16  
             f' 16  
             ees' 16  
             d' 8  
             c'' 16  
             b' 16  
             \bar "|"  %{ end measure 6 %} 
             c'' 4  
             r 4  
             r 8  
             f'' 8  
             ees'' 8  
             d'' 8  
             \bar "|"  %{ end measure 7 %} 
             r 8  
             aes' 8  
             g' 8  
             f' 8  
             g' 8  
             f' 16  
             ees' 16  
             f' 8  
             d' 8  
             \bar "|"  %{ end measure 8 %} 
             g' 4  
             r 8  
             b' 8  
             c'' 8  
             c'' 16  
             b' 16  
             c'' 8  
             g' 8  
             \bar "|"  %{ end measure 9 %} 
             aes' 4  
             r 8  
             a' 8  
             bes' 8  
             bes' 16  
             a' 16  
             bes' 8  
             f' 8  
             \bar "|"  %{ end measure 10 %} 
             g' 4  
             r 8  
             g' 8  
             aes' 8  
             aes' 8  
             g' 8  
             f' 8  
             \bar "|"  %{ end measure 11 %} 
             r 8  
             aes 8  
             bes 8  
             c' 8  
             r 8  
             aes 16  
             g 16  
             aes 8  
             f 8  
             \bar "|"  %{ end measure 12 %} 
             bes 8  
             c' 8  
             bes 8  
             aes 8  
             bes 8  
             g 8  
             f 8  
             ees 8  
             \bar "|"  %{ end measure 13 %} 
             f 8  
             des' 8  
             c' 8  
             bes 8  
             c' 8  
             aes 8  
             g 8  
             f 8  
             \bar "|"  %{ end measure 14 %} 
             g 8  
             g' 16  
             fis' 16  
             g' 8  
             c' 8  
             ees' 8  
             g' 16  
             fis' 16  
             g' 8  
             a' 8  
             \bar "|"  %{ end measure 15 %} 
             d' 8  
             g' 16  
             fis' 16  
             g' 8  
             a' 8  
             c' 16  
             d' 16  
             ees' 4  
             d' 16  
             c' 16  
             \bar "|"  %{ end measure 16 %} 
             bes 8  
             r 8  
             r 16  
             d' 16  
             e' 16  
             fis' 16  
             g' 16  
             a' 16  
             bes' 8  ~  
             bes' 16  
             e' 16  
             f' 16  
             g' 16  
             \bar "|"  %{ end measure 17 %} 
             a' 16  
             bes' 16  
             c'' 8  ~  
             c'' 16  
             fis' 16  
             g' 16  
             a' 16  
             bes' 8  
             ees' 16  
             d' 16  
             ees' 8  
             g 8  
             \bar "|"  %{ end measure 18 %} 
             aes 8  
             f' 16  
             ees' 16  
             f' 8  
             a 8  
             bes 8  
             g' 16  
             f' 16  
             g' 8  
             b 8  
             \bar "|"  %{ end measure 19 %} 
             c' 16  
             f' 16  
             ees' 16  
             d' 16  
             c' 16  
             bes 16  
             aes 16  
             g 16  
             f 8  
             aes' 8  
             g' 8  
             f' 8  
             \bar "|"  %{ end measure 20 %} 
             ees' 8  
             d' 8  
             ees' 8  
             f' 8  
             b 8  
             c' 8  
             d' 8  
             b 8  
             \bar "|"  %{ end measure 21 %} 
             c' 4  
             r 8  
             e' 8  
             f' 8  
             f' 16  
             e' 16  
             f' 8  
             c' 8  
             \bar "|"  %{ end measure 22 %} 
             d' 4  
             r 8  
             d' 8  
             ees' 8  
             ees' 16  
             d' 16  
             ees' 8  
             bes 8  
             \bar "|"  %{ end measure 23 %} 
             c' 2  ~  
             c' 8  
             d' 16  
             ees' 16  
             f' 16  
             ees' 16  
             f' 16  
             d' 16  
             \bar "|"  %{ end measure 24 %} 
             b 8  
             r 8  
             r 8  
             b 8  
             c' 8  
             r 8  
             r 8  
             ees' 8  
             \bar "|"  %{ end measure 25 %} 
             d' 8  
             r 8  
             r 8  
             f' 8  ~  
             f' 8  
             r 8  
             r 8  
             f' 8  
             \bar "|"  %{ end measure 26 %} 
             ees' 8  
             aes' 8  
             g' 8  
             f' 8  
             ees' 8  
             d' 8  
             ees' 8  
             f' 8  
             \bar "|"  %{ end measure 27 %} 
             b 8  
             c' 8  
             d' 8  
             b 8  
             b 8  
             c' 8  
             r 8  
             c' 8  
             \bar "|"  %{ end measure 28 %} 
             f' 16  
             d' 16  
             ees' 16  
             c' 16  ~  
             c' 8  
             b 8  
             c' 4  
             r 8  
             e' 8  
             \bar "|"  %{ end measure 29 %} 
             f' 4  
             r 8  
             < f'  aes'  > 8   
             f' 8  
             ees' 16  
             d' 16  
             ees' 8  
             < f'  aes'  > 8   
             \bar "|"  %{ end measure 30 %} 
             < b  d'  > 8   
             r 8  
             < b  d'  > 8   
             r 8  
             < g  c'  > 2   
             \bar "||"  %{ end measure 31 %} 
              } 
            
 
       \new Staff  = spinerw { \clef "bass" 
             \key ees \major 
             \time 4/4
             \key c \minor 
             r 1  
             \bar "|"  %{ end measure 1 %} 
             r 1  
             \bar "|"  %{ end measure 2 %} 
             r 1  
             \bar "|"  %{ end measure 3 %} 
             r 1  
             \bar "|"  %{ end measure 4 %} 
             r 1  
             \bar "|"  %{ end measure 5 %} 
             r 1  
             \bar "|"  %{ end measure 6 %} 
             r 8  
             c' 16  
             b 16  
             c' 8  
             g 8  
             aes 8  
             c' 16  
             b 16  
             c' 8  
             d' 8  
             \bar "|"  %{ end measure 7 %} 
             g 8  
             c' 16  
             b 16  
             c' 8  
             d' 8  
             f 16  
             g 16  
             aes 4  
             g 16  
             f 16  
             \bar "|"  %{ end measure 8 %} 
             ees 16  
             c' 16  
             b 16  
             a 16  
             g 16  
             f 16  
             ees 16  
             d 16  
             c 16  
             d 16  
             ees 16  
             d 16  
             c 16  
             bes, 16  
             aes, 16  
             g, 16  
             \bar "|"  %{ end measure 9 %} 
             f, 16  
             bes 16  
             aes 16  
             g 16  
             f 16  
             ees 16  
             d 16  
             c 16  
             bes, 16  
             c 16  
             d 16  
             c 16  
             bes, 16  
             aes, 16  
             g, 16  
             f, 16  
             \bar "|"  %{ end measure 10 %} 
             ees, 16  
             aes 16  
             g 16  
             f 16  
             ees 16  
             des 16  
             c 16  
             bes, 16  
             aes, 8  
             c' 8  
             bes 8  
             aes 8  
             \bar "|"  %{ end measure 11 %} 
             g 8  
             f 8  
             g 8  
             aes 8  
             d 8  
             ees 8  
             f 8  
             d 8  
             \bar "|"  %{ end measure 12 %} 
             ees 8  
             aes 8  
             g 8  
             f 8  
             g 8  
             ees 8  
             d 8  
             c 8  
             \bar "|"  %{ end measure 13 %} 
             d 8  
             bes 8  
             aes 8  
             g 8  
             aes 8  
             f 8  
             ees 8  
             d 8  
             \bar "|"  %{ end measure 14 %} 
             ees 8  
             r 8  
             r 4  
             r 8  
             c 8  
             bes, 8  
             a, 8  
             \bar "|"  %{ end measure 15 %} 
             r 8  
             ees 8  
             d 8  
             c 8  
             d 8  
             c 16  
             bes, 16  
             c 8  
             d 8  
             \bar "|"  %{ end measure 16 %} 
             g, 8  
             bes 16  
             a 16  
             bes 8  
             d 8  
             ees 8  
             c' 16  
             bes 16  
             c' 8  
             e 8  
             \bar "|"  %{ end measure 17 %} 
             f 8  
             d' 16  
             c' 16  
             d' 8  
             fis 8  
             g 4  
             r 16  
             g, 16  
             a, 16  
             b, 16  
             \bar "|"  %{ end measure 18 %} 
             c 16  
             d 16  
             ees 8  ~  
             ees 16  
             a, 16  
             bes, 16  
             c 16  
             d 16  
             ees 16  
             f 8  ~  
             f 16  
             b, 16  
             c 16  
             d 16  
             \bar "|"  %{ end measure 19 %} 
             ees 8  
             r 8  
             r 8  
             e 8  
             f 8  
             f, 8  
             ees, 8  
             d, 8  
             \bar "|"  %{ end measure 20 %} 
             r 8  
             aes, 8  
             g, 8  
             f, 8  
             g, 8  
             f, 16  
             ees, 16  
             f, 8  
             g, 8  
             \bar "|"  %{ end measure 21 %} 
             c 16  
             d 16  
             ees 16  
             d 16  
             c 16  
             bes, 16  
             aes, 16  
             g, 16  
             f, 16  
             bes 16  
             aes 16  
             g 16  
             f 16  
             ees 16  
             d 16  
             c 16  
             \bar "|"  %{ end measure 22 %} 
             bes, 16  
             c 16  
             d 16  
             c 16  
             bes, 16  
             aes, 16  
             g, 16  
             f, 16  
             ees, 16  
             aes 16  
             g 16  
             f 16  
             ees 16  
             d 16  
             c 16  
             bes, 16  
             \bar "|"  %{ end measure 23 %} 
             aes, 16  
             bes, 16  
             c 16  
             bes, 16  
             aes, 16  
             g, 16  
             f, 16  
             ees, 16  
             d, 16  
             g 16  
             f 16  
             ees 16  
             d 16  
             c 16  
             b, 16  
             a, 16  
             \bar "|"  %{ end measure 24 %} 
             g, 4  
             r 4  
             r 16  
             g, 16  
             a, 16  
             b, 16  
             c 16  
             d 16  
             ees 16  
             f 16  
             \bar "|"  %{ end measure 25 %} 
             g 16  
             f 16  
             aes 16  
             g 16  
             f 16  
             ees 16  
             d 16  
             c 16  
             b, 8  
             c 16  
             b, 16  
             c 8  
             g, 8  
             \bar "|"  %{ end measure 26 %} 
             aes, 8  
             c 16  
             b, 16  
             c 8  
             d 8  
             g, 8  
             c 16  
             b, 16  
             c 8  
             d 8  
             \bar "|"  %{ end measure 27 %} 
             f, 16  
             g, 16  
             aes, 4  
             g, 16  
             f, 16  
             ees, 4  
             r 8  
             ees 8  
             \bar "|"  %{ end measure 28 %} 
             d 8  
             c 8  
             g 8  
             g, 8  
             < c,  c  > 2  ~   
             \bar "|"  %{ end measure 29 %} 
             < c,  c  > 1  ~   
             \bar "|"  %{ end measure 30 %} 
             < c,  c  > 1   
             \bar "||"  %{ end measure 31 %} 
              } 
            
 
        >>
      
  } 
 
\paper { }
\layout {
  \context {
    \RemoveEmptyStaffContext
    \override VerticalAxisGroup #'remove-first = ##t
  }
 }
 
