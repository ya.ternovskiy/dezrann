


## Grille

Toutes les opérations relative à la grille se font si une grille est sélectionnée/activée (#60).
Un clic long et le fait de laisser appuyer le bouton de la souris pendant une tempo à définir tout en laissant le pointeur sur le label.


## Sélection d'un label (ou grob)

Pour sélectionner un label il faut faire un clic long ? court ? dessus (à discuter, #362).
En retour utilisateur, le label change d'apparence:

- pour les rectangle, une bordure et les 'resizers' sont ajoutés.
- pour les lignes verticales, l'opacité augmente.

La sélection d'un label ne change pas ses coordonnées, mêmes si elles ne sont pas sur la grille.

## Création d'un label instantané (ou vertical ligne) par clic/tap.

Un clic long au même endroit sur une ligne produit la création d'un label instantané
à la position référencée par la grille la plus proche.
Si le bouton est relâché (up) ou quele doigt n'est plus en contact cela termine la phase de création.
Le label ainsi créé est LE grob sélectionné (opacité forte).

## Création d'un grob

Un clic long au même endroit sur une ligne produit la création d'un label instantané à la position référencée par la grille la plus proche.
Tout en laissant le bouton appuyé si on déplace le curseur vers la droite ou vers la gauche, un rectangle est créé et sa largeur évolue en fonction des déplacements du pointeur.
Cette largeur correspond à la distance entre la position du curseur au clic long (point de départ) et la position courante du curseur; le tout modulo les positions référencées par la grille, si la grille est activée.
Si cette distance est nulle, c'est une VL qui est affichée (et créée). Lorsque le bouton est relâché, la phase de création se termine. Le grob ainsi créé est LE grob sélectionné.

## Création d'un grob sur plusieurs lignes

Un clic long au même endroit sur une ligne produit la création d'une VL à la position référencée par la grille la plus proche.
Tout en laissant le bouton appuyé si on déplace le curseur sur une autre ligne en haut ou en bas, le grob recouvre l'ensemble des lignes se trouvant
entre la position de départ (clic long) et la position courante du curseur (lignes extrêmes incluses).
Une VL ou un rectangle est affiché selon que la distance horizontale parcourue est nulle ou non (comme expliqué dans le paragraphe précédent).
Lorsque le bouton est relâché, la phase de création se termine. Le grob ainsi créé est LE grob sélectionné.

### Limitation actuelle des labels

- Un label instantané peut être créé sur une ou toutes les lignes, pas sur une partie des lignes.
  Si le curseur se déplace verticalement sur plusieurs lignes il est créé sur toutes les lignes et pas uniquement sur celles parcourues.
- Un label de durée non nulle ne peut être créé que sur une ligne. Si le déplacement horizontal est nul il devient un label instantané
  qui s'étend sur toutes les lignes si plusieurs lignes ont été parcourues depuis le point de départ.

## Déplacement d'un grob

Pour déplacer un grob il doit soit être sélectionné avant de déplacer le curseur. Il suffit ensuite de faire un drag un drop dessus. A la fin du déplacement le grob reste sélectionné.

## Redimensionnement d'un grob

Seul un rectangle sélectionné est redimensionnable horizontalement car il possède des resizers. Il suffit de rester appuyer sur un resizer et de déplacer le curseur. A la fin du redimensionnement le grob reste sélectionné.
