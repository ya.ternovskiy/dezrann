# Réflexion sur la sécurité

## Contrôle d'accès

### Les ressources à protéger

#### Les corpus

#### Les pièces

#### Les analyses

### Les droits

#### Lire un corpus

#### Ajouter un corpus

#### Supprimer un corpus

Seul le propriétaire d'un corpus peut le supprimer.

#### Ajouter une pièce

#### Supprimer une pièce

#### Lire une pièce

#### Modifier une pièce

#### Lire une analyse

#### Ajouter une analyse

#### Supprimer une analyse

#### Sauvegarder une analyse sur le serveur


### Notion de propriétaire

Un utilisateur qui a le droit de créer une ressource (corpus, pièce ou analyse)
en est le propriétaire. S'il a le droit de suppression, il peut supprimer la ressource.
Il peut
éventuellement désigner d'autres utilisateurs ou groupes propriétaires de la
ressource. Tous les utilisateurs d'un groupe désigné propriétaire seront
considérés comme tel.

-> Certains ne pourront même pas supprimer ce qu'ils ont créé (élève).
--> voir ce qu'il est plus judicieux de faire en classe. Le prof pourrait
alors régler ce droit avant la séance.
-> oui

Cela peut être même le comportement par défaut de tout nouvel utilisateur.
--> un nouvel utilisateur doit pouvoir supprimer sinon il risque de se
retrouver avec plein de ressources qu'il a créées pour tester.
--> oui

Le propriétaire d'une ressource ne pourra la supprimer que si tous les enfants
lui appartiennent. Par exemple, si Bob est propriétaire d'une pièce, il n'est
pas propriétaire de l'analyse créée par Alice. Il ne pourra supprimer ni
l'analyse d'Alice ni la pièce dont elle dépend.

A y réflèchir (cas du travail en classe), peut-être pas, le propriétaire devrait
peut-être porvoir supprimer quand même. Il pourrait-être simplement averti.
Il pourrait être averti que le corpus n'est pas vide.

-> Très bonnes questions. Cela peut être deux droits différents :
un "delete-my" et un autre "delete-my-and-othes"
Par défaut, on ne mettrait que "delete-my" et même peut-être... rien du tout au début,
pas de suppression. Qui (quel rôle) fixe ces droits? admin?

-> Au final, en plus des droits "delete", le rôle "owner" a surtout
des droits pour donner accès à sa ressource "share" ?

Il y a aussi le pb de droits. Le propriétaire peut changer les droits du corpus
par exemple et rendre inaccessible son corpus à des utilisateurs qui
l'utilisaient (avait créer des analyses). Faut-il interdire? Simplement avertir?

-> Avertir peut être suffisant.

A la suppression ou au changement de droits d'une ressource, on peut éventuellement avertir
aussi les utilisateurs dont l'accès à la ressource a changé.

-> Mais pas systématiquement. Le prof n'a pas forcément envie que les élèves voient toutes ses manips.
En tant qu'administrateur d'un corpus, je n'ai pas non plus forcément envie qu'un utilisateur
voit que je change des choses.
--> dans le cas où la ressource qu'a créé un utilisateur lui est inaccessible,
il doit savoir pourquoi ou tout au moins être averti que c'est normal.
--> oui... mais légèrement (pas de mail). Une manière de l'avertir serait qu'il puisse toujours voir et accéder à sa ressource
(il voit qu'il a une analyse, qu'il peut même charger en .dez chez lui), mais on lui dit qu'il ne plus plus accéder à la pièce.
Il pourrait alors créer son propre corpus/piece. From scratch ou en faisant un
export/import de la piece (equivallent à un déplacement). Ce déplacement pourrait
se faire automatiquement au besoin. L'utilisateur recevrait alors un message dans
sa page de profil du genre:
"votre analyse a été deplacée à tel endroit car le corpus intel a été supprimé."
--> oui

### Profil utilisateur

```js
[
  {
    uid : "mathieu",
    firstname : "Mathieu",
    lastname : "Giraud",
    mail: "mathieu@algomus.fr",
    password : "secret"
  },
  {
    uid : "manu",
    firstname : "Emmanuel",
    lastname : "Leguy",
    mail: "manu@algomus.fr",
    password : "secret"
  },
  {
    uid : "collegien1",
    firstname : "Collegien",
    password : "secret"
  },
  {
    uid : "toto49",
    password : "secret"
  }
  ...
]
```

Le mail et les noms sont optionnels: on peut avoir des comptes génériqus, déjà créés (pour des
invités ou des élèves). On pourra probablement se logguer aussi par token.
--> à discuter comment le token est transmis (verbalement? au tableau? dans un composant?).

À voir effectivement. Quelque chose d'aussi simple que '8tz5a5' à mettre quelque part, et/ou un lien 'http://dezrann.net/8tz5a5' ?
Après certains profs mettront cela dans un lien depuis des pages à eux, d'autres au tableau.

Cela peut être aussi utile avec des collègues, pour leur donner directement un truc où ils arrivent et peuvent annoter en analysis-write.

### Groupes

```js
[
  {
    id : "algomus",
    name : "Algomus",
    members : ["mathieu", "manu", "richard", "louis", "florence"]
  },
  {
    id : "edmus",
    name : "Éducation musicale au collège",
    members : ["edmus-amiens", "bernard"]
  },
  {
    id : "edmus-amiens",
    name : "Éducation musicale au collège, académie d'Amiens",
    members : ["francois", "vincent"]
  },
  {
    id : "collegiens",
    name "Collégiens",
    members : ["collegien1", "collegien2", ... , "collegienn"]
  }
]
```

Les membres des groupes peuvent être des utilisateurs ou d'autres groupes.

### Ressources

--> voir access.json

#### droits / permissions

- lire: read-piece, read-analysis
- modifier: add-piece, add-analysis, write-analysis (impliquent read-piece / read-analysis)
- supprimer: delete-my, delete-my-and-other
- share: partager avec d'autres utilisateurs/groupes (dans la limite des droits qu'on a).
La liste des utilisateurs/groupes proposés à l'utilisateur pour partager sa
ressource doit être cohérente. Ne pas rendre possible le partage d'une ressource
inaccessible.
-> oui, tout à fait

Certaines personnes peuvent avoir le droit d'écrire des analyses sur tout un corpus, sans avoir
de droit de modifier les pièces du corpus : add-analysis sur le corpus.
(Ce serait même a priori le défaut pour des corpus que l'on met à disposition
et pour lesquels les personnes peuvent faire des analyses... mais sans rien pertuber aux autres analyses,
peut-être même sans y accéder !)
Distinction add-analysis: ajouter des analyses / write-analysis: modifier les analyses des autres
(similaire à delete-my / delete-my-and-other ? Y-a-t-il encore besoin de delete ? En droits UNIX, pas
de droits de suppression, si je peux modifier je peux supprimer)
Il me semble que c'est au propriétaire de l'analyse de décider qui a le droit de
la modifier et/ou de la supprimer.
-> pas pour l'élève... et même entre collaborateurs: dans "algomus"
ou bien dans "collaboration-sorbonne" on souhaite pouvoir tous modifier les analyses créées par l'un ou l'autre des membres à l'intérieur
d'un corpus. (Une solution peut être qu'on soit tous 'propriétaires', à voir ce que cela recouvre.

Idem pour les droits de lecture: on peut donner (ou pas) le droit de lecture à l'ensemble des analyses d'un corpus.

#### roles

- admin : a tous les droits
- owner : ['write-piece', 'write-analysis', 'delete-my', 'share']
- writer = ['write-piece', 'write-analysis']
- reader = ['read-piece', 'read-analysis']


Toutes ces clés seraient optionnelles: si fugue/bwv848 n'existe pas, on a les permissions de fugue/.


## Cas d'utilisation

- vérifier qu'un utilisateur a le droit d'accéder à une ressource.
- liste des analyses qu'un utilisateur peut lire/modifier/supprimer?
- liste des pièces qu'un utilisateur peut lire/modifier/supprimer?
- liste des corpus qu'un utilisateur peut lire/modifier/supprimer?
- créer une analyse/piece/corpus
- partager une analyse/piece/corpus
- fermer une analyse/piece/corpus
- supprimer analyses
- supprimer piece
- supprimer corpus
- ajouter un utilisateur
- ajouter un groupe
- suppression d'un utilisateur?
- suppression d'un groupe?

### Créer un corpus

#### Créer un corpus en tant qu'admin

- créer url du corpus: https://dezrann.net/new_corpus
- fixer les droits:
  - les utilisateurs ayant le role 'own' sont propriétaires du corpus
  - personne d'autre ne peut y accéder

#### Créer un corpus de groupe

- créer url du corpus: https://dezrann.net/group_id/new_corpus
- fixer les droits:
  - le propriétaire est le role 'own' du groupe
  - les membres du groupe peuvent ajouter/modifier/supprimer une piece/analyse

#### Créer un corpus en tant qu'utilisateur

- créer url du corpus: https://dezrann.net/user_id/new_corpus
- fixer les droits:
  - l'utilisateur est le proprétaire du corpus
  - personne d'autre ne peut y accéder

## Questions

Question subsidiaire : est-ce que le info.js aura sa place dans un tel modèle (que ce soit le premier ou le troisième) ?

    pieces: [
      {
        id: "bwv846",
        title: "xxxxx",
        sources: "xxxxxx",
        audio: "xxxxxx",        
        ...
        access: [ ("write-analysis", ["bernard"]) ]
      }, ... ],                
