# Un réseau social pour annoter la musique (suite)

L'équipe Algomus développe l'application web 'Dezrann' pour lire et annoter
des partitions musicales (prototype sur http://dezrann.net/). L'annotation se
fait en ajoutant des éléments graphiques sur la partition: les 'labels'. Cette
application utilise le framework Polymer qui implémente la technologie des
*Web Components* (standard du W3C). Nous réalisons un ensemble de balises
HTML paramétrables qui s'intègrent aisément dans une page web à la manière des
balises HTML5 vidéo ou audio.

L'objectif du projet est de pouvoir annoter à plusieurs et en temps réel une
partition de musique. Un travail préliminaire, utilisant la technologie des
'WebSocket', a permis que les manipulations de labels (création, déplacements,
agrandissements...) effectuées par un utilisateur soit visibles instantanément
par les autres. Ce travail doit maintenant être amélioré sur 2 points:

1- Centraliser l'état de l'analyse. Lors d'une session de travail à plusieurs il
faut s'assurer que chaque participant visualise le même état de l'analyse. Le
système doit être tolérant aux pannes réseau (latence ou perte de connexion). Un
nouveau participant doit pouvoir également recupéré la dernière version de
l'analyse. Cela signifie que l'état de l'analyse doit être centralisé

2- Implémenter la notion de canal. Un canal est une session de travail à
plusieurs. Un utilisateur identifié doit pouvoir ouvrir un canal, inviter
d'autres personnes ou rejoindre un canal déjà ouvert.

Le code sera écrit avec grand soin, documenté et testé. En cas de succès du
projet, l'application étendue sera testée en situation réelle dans des classes
d'éducation musicale de l'académie d'Amiens.

Mots clés : partitions musicales, Polymer, websocket, socket.io, javascript.

Liens :
 - http://dezrann.net/
 - https://socket.io/
 - https://www.polymer-project.org/
