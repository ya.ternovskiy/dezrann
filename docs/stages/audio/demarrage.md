# Demarrage du PJI: vendredi 20 janvier 2017

## Jouer/interagir avec un fichier audio (mp3, ogg, wav...)

- synchro audio/partition
- une ou plusieurs versions
- multi-piste: un fichier par voix
- caler l'audio à un temps donné (en fonction de la position du curseur graphique)
- contrôler le défilement de la partition:
    - envoyer un évènement toutes les x secondes
- outils:
    - Web audio api
    - balise `<audio>`
    - ce qu'apporte audio5js

## Produire/jouer du son à partir de notes (fréquence (hauteur), durée, instrument)

- isoler les voix: jouer un sous-ensemble de voix en même temps.
- jouer un extrait de voix
- outils:
    - web audio api
    - Web MIDI api
    - midi.js
