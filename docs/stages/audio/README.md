# Partitions en ligne : on monte le son !

L'équipe Algomus développe une application web pour lire et annoter des partitions musicales. Cette application utilise le framework Polymer qui implémente la technologie des *WEB COMPONENTS* (futur standard du W3C). Nous réalisons un ensemble de balises HTML paramétrables qui s'intègrent aisément dans une page web à la manière des balises HTML5 vidéo ou audio.

L'objectif du projet est de gérer la production sonore dans l'application, pour pouvoir écouter :

- soit la musique provenant d'un fichier audio (ogg, mp3, wav, flac...),
- soit une voix isolée de l'oeuvre, le son étant généré par le navigateur à partir de la partition.

## Analyse

### Protocole MIDI

Le Musical Instrument Digital Interface ou MIDI est un protocole de communication et un format de fichier dédiés à la musique, et utilisés pour la communication entre instruments électroniques, contrôleurs, séquenceurs et logiciels de musique.

>
Il y a 3 formats différents de fichiers MIDI :
>
1. : une seule piste contenant les messages des 16 canaux ;
2. : plusieurs pistes jouées simultanément ;
3. : plusieurs pistes jouées séquentiellement (rarement utilisé).
>
-- <cite> [Wikipedia](https://fr.wikipedia.org/wiki/Musical_Instrument_Digital_Interface)

Grâce au deuxième format nous pouvons séparer les pistes d'une partition pour pouvoir les lire séparément à la demande de l'utilisateur.

### Soundfont

>
SoundFont est une technique mise au point par la société E-mu pour Creative Labs, permettant de stocker dans un fichier au format .sbk (pour SoundFont Bank) puis .sf2 (pour SoundFont Bank Version 2) des échantillons au format WAV, puis de les organiser sous forme d'instruments MIDI, afin que ceux-ci puissent être utilisés sur un synthétiseur dit à tables d'ondes.
>
-- <cite> [Wikipedia](https://fr.wikipedia.org/wiki/SoundFont)

### General MIDI

>
General MIDI est une norme visant à améliorer la compatibilité des instruments de musique électroniques utilisant le protocole MIDI. Elle établit entre autres une numérotation des timbres que peut jouer l'instrument et leur classification par familles d'instruments (les claviers/les guitares/les cuivres/les nappes/etc.). Il est donc possible d'échanger des fichiers General MIDI sans connaître le matériel dont dispose le destinataire, ce qui permet le respect des timbres choisis pour chaque piste MIDI par le fabricant du fichier. Dans un fichier MIDI qui ne respecterait pas la norme General MIDI, une piste programmée avec un timbre de guitare pourrait sonner par exemple avec un timbre de trompette sur un autre appareil MIDI.
>
-- <cite> [Wikipedia](https://fr.wikipedia.org/wiki/General_MIDI)

### Scénarios

- [ ] TODO
 
### Cas d'utilisation

- [ ] TODO

### Technologies existantes

Voici un résumé des technologies web existantes.

- [ ] TODO

#### Web Audio API

>
La Web Audio API propose un système puissant et flexible pour contrôler les données audio sur internet. Elle permet notamment de sélectionner des sources audio (microphone, flux media), d'y ajouter des effets, de créer des visualisations, d'appliquer des effets de spatialisation (comme la balance), etc.

>
-- <cite>[Mozilla Developer Network](https://developer.mozilla.org/fr/docs/Web/API/Web_Audio_API)</cite>

En d'autres termes, la Web Audio API offre un ensemble d'outils dont le fonctionnement est comparable à une table de mixage.

- Multiples sources audio en entrée
- Ajout d'effets audio
- Multiples sources audio en sortie

- [ ] TODO

#### Web MIDI API

La Web MIDI API propose des outils permettant aux développeurs web de manipuler et d'accéder aux périphériques MIDI logiciel et matériel. Cette API permet donc à des applications Web de communiquer avec des synthétiseurs, des contrôleurs de musique ou d'éclairage et d'autres appareils à travers le protocole MIDI.

Les approches adoptées par cette API sont similaires à celles prises par l'API CoreMIDI d'Apple et Windows MIDI API de Microsoft; qui est, l'API conçu pour représenter le protocole MIDI logiciel bas niveau, afin de permettre aux développeurs de créer des logiciels MIDI de niveau suppérieur. L'API permet au développeur d'énumérer les interfaces d'entrée et de sortie, et d'envoyer et de recevoir des messages MIDI.
Cette API ne tente pas de définir ou d'interpréter sémantiquement des messages MIDI au-delà de ce qui est nécessaire à la communication.

L'API MIDI Web ne vise pas à mettre directement en œuvre des concepts de haut niveau tels que le séquençage audio. Par exemple, l'API ne supporte pas directement les fichiers Standard MIDI bien qu'un lecteur de fichier MIDI standard peut être développé au-dessus de la Web MIDI API.

#### MIDI.js

- [ ] TODO

[Github](https://github.com/mudcube/MIDI.js)

#### MIDI.js Soundfonts

MIDI.js Soundfonts est une bibliothèque d'instruments utilisables avec MIDI.js respectant la norme General MIDI.

[Github](https://github.com/gleitz/midi-js-soundfonts)

## Conception

## Développement