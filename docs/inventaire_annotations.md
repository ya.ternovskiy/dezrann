# Inventaire des annotations

## général

- cadence. Label global.
- pédale
- degrés
- marche harmonique courte et longue
- structure: exposition, developpement...

## Fugue

- S
- cs
- cs2
- debut de sujet

## Texture

- hammer, coup de marteau: 3 accords (noire) + silence. Label global.
- bsse chromatique
- mouvement parallèle
- silence: du silence sur toutes les voix
- unisson

## Sonate

- Cesure médiane
- thème P et T (rupture non franche)
- theme S
- conclusion
