# Cas d'utilisation

## Naviguer

### Naviguer dans la partition à l'aide du schema

Le schema est une vue synthétique de la partition et surtout de son analyse.
Pour l'instant, il doit toujours être visible dans son intégralité. Il est ainsi un moyen de se
situer rapidement dans la pièce. Il est utilisé à la manière de la vue réduite
de sublimetext pour naviguer aisément dans la partition. Par exemple en
cliquant au milieu du schéma, la vue partition est instantanément mise à jour
pour l'afficher au milieu. Pour aider l'utilisateur à faire le lien entre les 2
vues, une zone rectangulaire transparente représentant la 'fenêtre' courante de
la partition sera affichée sur le schema.

### Naviguer dans la partition à l'aide d'une vue entiere réduite avec fenetre courante

Similaire à l'utilisation du schema. Pas utile, trop rendondant.

### Naviguer dans la partition en déplaçant directement la partition (drag an drop).

La navigation par la vue schema n'est peut-être pas suffisante. A voir...
Il ne faut pas être en mode édition pour faire le drag and drop pour ne pas modifier
l'analyse. Plusieurs moyens:
* un bouton de basculement de mode
* appui simultané sur la touche ctrl
* 2 doigts, appui long...
* ...

## Dessiner

### Dessiner dans la partition un label

La partition possède un quadrillage invisible composée de lignes correspondant
aux portées. Chaque lignes est composée de colonnes correspondant aux positions
des notes et des barres de mesure.
Il y a des zones où l'on ne peut pas dessiner:
* inter-portée (sauf pour les labels sur plusieurs voix. Rem: pour une pièce à
  3 labels sur 2 voix)
* à gauche de la métrique principale. rem: un label peut-il être à cheval de 2
  mesures
  ayant des métrique, clef, armure différentes. Besoin de définir des contraintes de dessin
  particulière?

Pour dessiner un label de portée:
* (Être dans un mode édition)
* Entourer la zone de portée en cliquant sur un point de départ et en relachant à un
  point d'arrivée.
* Nommer le label / sélectionner une couleur / éditer (tout cela est facultatif)

Rem: possibilité de sélectionner une catégorie de label: Sujet, Contre-sujet, CS2, CS3,
Thème principal... Accès à un catalogue de catégories. Une couleur est fixée.
Peut-être quelques catégories par défaut (Label 1, Label 2, Label 3), avec trois couleurs.
Le type (catégorie) est nécessaire pur un fichier truth.
Comment gère-ton l'absence de type?
Possibilité de définir des types en ligne (custom)?

Le label est également dessiné sur le schema.

Tout label est éditable, peut-être même en mode texte (ce qui peut le faire sortir de la grille par voix)

#### Cas du label global à toutes les portées

Zone en haut et/ou en bas cliquable, éventuellement matérialisée (grisé ? :hover ? pointeur ?)

Un (des) geste(s) identique pour tous les trucs globaux

##### Les Cadences // instatanné / Annotations générale (ex: degré) en haut ou en bas

Plusieurs possibilité:
* Placer le curseur au bon endroit et appuyer sur un bouton de création de cadence.
* Cliquer droit au bon endroit et sélectionner cadence.
* Passer en mode création de cadence et déssiner la cadence
* Dessiner une ligne verticale en partant du haut (ou du bas?) et à l'abscisse voulue.
  Ajustement automatique (calage).

##### Les rectangle (pédales, sections (ex: exposition))

* Dessiner un rectangle en partant d'un coin.

#####

#### Implémentation

Lorsqu'on clique en restant appuyé (mouse down):
* Mise à jour de l'état de la navigation à partir des données de la partition graphique (image ou vexflow)
  * partitionGraphique.getOffset()
  * partitionGraphique.getStaff()
  * partitionGraphique.getMesure()
* si image (ex: produite par lilypond)
  * imageScore.getOffset(x,y)
  * imageScore.getStaff(x,y)
  * imageScore.getMesure(x,y)
* si vexflow

### Dessiner dans le schema

Utile si partition grande.
Ce n'est pas en cliquant dans le schéma qu'on va viser une croche/noire, mais plutôt par mesure.


## Visualiser plusieurs analyses, basculer

Calques, load et save
-> après, pour l'instant : 1 analyse qu'on charge et qu'on enregistre

## Visualiser le fichier truth

## Visualiser un label

### Visualiser un label en cliquant sur la partition
### Visualiser un label en cliquant sur le schema

## Ecouter un label, une section, une ou plusieurs voix
## Ajuster un label (entrer l'offset exact)

## Ouvrir une analyse ++

* Ouvrir la piece:
  * image + repère:
    * ouvrir image
    * ouvrir positions
  * vexflow
* Ouvrir l'analyse

## Sauver une analyse ++

Faire une analyse, c'est créer des labels. Ces labels sont regroupés par ligne
d'analyse (objectif d'analyse ou calque (layer)):
* analyse de voix
* recherche des sujets
* recherche des marches harmoniques
* degrés
* cadences parfaites
* demi-cadences
* pédales
* fin de phrase
* changement de texture
* ...
Une analyse est donc un ensemble de lignes (ou de groupe de labels)
* possibilité de composer plusieurs analyses à partir de lignes provenant
  d'autres analyse: par exemple, on prend les cadences de l'analyse A et les
  sujet de l'analyse B...
* comparaison de ligne d'analyse.

La structure de données sous-jacente doit donc tenir compte de cette hierarchie.
Cela va plus loin que les schemas music21. En effet, il y a soit des calques
attachés aux voix, soit des calques globaux. Comment représenter par exemple un
calque des sujets qui est global à la pièce mais dont les labels sont attachés à
une voix. Besoin d'une abstraction permettant plus de flexibilité.

Afficher/effacer tous les sujets me parait plus utile que d'afficher/effacer
tous les labels d'une voix.

## Ouvrir une partition

### Ouvrir un fichier image + fichier de repérage ++

Ce sont les données brutes. -> pour l'instant, parfait

* l'utilisateur a préalablement produit une image png ou jpg de la partition et
  repéré les positions des portées, mesures et notes (par portée). Le tout a eté
  sauvegardé dans un fichier. L'utilisateur est donc en posséssion d'un fichier
  image (png/jpg) et d'un fichier de repérage (json à priori)
* l'utilisateur charge ces 2 fichiers dans l'application:
  * l'IHM est munie d'un bouton '+' pour ajouter une pièce
  * un formulaire propose plusieurs type d'ajout:
    * brut (image + repérage)
    * fichier lilypond: génération des fichiers image + repérage en backend
    * fichier musicaux (krn, midi, musicXML, MEI...): génération d'un fichier
      lilypond intermédiaire puis génération final soit:
      * d'un fichier image + repérage
      * de code vexflow
* l'utilisateur choisit l'option 'fichiers bruts'
* l'utilisateur renseigne les méta-données: nom de la pièce, compositeur...
* l'utilisateur uploade le fichier image et le fichier de repérage
* l'image de la partition s'affiche dans zone visualisation dédiée à la partition
* une vue réduite vierge de l'analyse s'affiche dans la zone supérieure
* l'utilisateur peut commencer à éditer et à naviguer.


### Ouvrir un fichier lilypond

* besoin de reformatage du fichier lilypond: ajout des macros, autres formatages...
* génération de l'image
* génération du repérage

### Ouvrir un fichier krn, midi, musicXML, ou MEI

#### pour obtenir une images repérée:

* génération du ly (music21?)
* génération de l'image
* génération du repérage

#### obtenir un vexflow

## Analyser

* obligation de stocker les notes. OK pour krn, musicXML, MEI ou midi (?). Plus difficile pour lilypond.

## Analyser de l'audio (on sort du cadre d'algomus?)

## Switcher partition/schema

La zone principale représente par défaut la partition. Une zone secondaire affiche
le schema. Il est possible d'inverser cette disposition: zone principale -> schema
et zone secondaire -> partition. La vue située dans la zone principale peut-être
éditée plus facilement. La zone principale est-elle la seule zone d'édition?

## Definir le cadrillage d'une image de partition.
